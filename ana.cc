#include "rdftest1.cc"
#include "TreeSliceDS.cc"


const double M_Z = 91.19;


//**********************************************************************

class DressedLepton
  : public FourVec
{
public:
  DressedLepton() {}

  DressedLepton (const MCPart& p)
    : FourVec (p),
      m_typ (p.typ())
  {
  }

  DressedLepton (const RecoPart& p)
    : FourVec (p),
      m_typ (p.typ())
  {
  }

  void dress (const MCPart& v)
  {
    *this += v;
  }

  int typ() const
  {
    return m_typ;
  }


private:
  int m_typ = 0;
};


std::vector<DressedLepton> dress (const std::vector<MCPart>& leps,
                                  const std::vector<MCPart>& leps2,
                                  const std::vector<MCPart>& gams)
{
  std::vector<DressedLepton> ret;
  for (const MCPart& l : leps) {
    ret.emplace_back (l);
  }
  std::vector<MCPart> all_leps = leps;
  all_leps.insert (all_leps.end(), leps2.begin(), leps2.end());
  for (const MCPart& g : gams) {
    double min_dr2 = 0.1*0.1;
    size_t min_i = all_leps.size();
    for (size_t i = 0; i < all_leps.size(); i++) {
      float dr2 = ROOT::Math::VectorUtil::DeltaR2 (g, all_leps[i]);
      if (dr2 < min_dr2) {
        min_dr2 = dr2;
        min_i = i;
      }
    }
    if (min_i < leps.size()) {
      ret[min_i].dress (g);
    }
  }
  return ret;
}


//**********************************************************************

template <class CONT>
bool massWindow (const CONT& cont, float min_mass, float max_mass)
{
  for (const auto& rp : cont) {
    if (rp.M() >= min_mass & rp.M() <= max_mass) return true;
  }
  return false;
}


class LPair
  : public FourVec
{
public:
  using Lepton = DressedLepton;
  LPair ():m_typ(0),  m_i(0), m_j(0) {}
  LPair (size_t i, size_t j, const Lepton& ipart, const Lepton& jpart);
  int typ() const { return m_typ; }
  size_t i() const { return m_i; }
  size_t j() const { return m_j; }
  const Lepton& ipart() const { return m_ipart; }
  const Lepton& jpart() const { return m_jpart; }

private:
  int m_typ;
  size_t m_i;
  size_t m_j;
  Lepton m_ipart;
  Lepton m_jpart;
};


LPair::LPair (size_t i, size_t j, const Lepton& ipart, const Lepton& jpart)
  : FourVec (ipart + jpart),
    m_typ (std::abs (ipart.typ())),
    m_i (i),
    m_j (j),
    m_ipart (ipart),
    m_jpart (jpart)
{
}


// opposite-sign lepton pairs.
template <class PART>
std::vector<LPair> make_lpairs (const std::vector<PART>& rps)
{
  std::vector<LPair> ret;
  size_t sz = rps.size();
  if (sz > 0) {
    for (size_t i = 0; i < sz-1; ++i) {
      LPair::Lepton ipart = rps[i];
      for (size_t j = i+1; j < sz; ++j) {
        LPair::Lepton jpart = rps[j];
        // Signs of leptons in pair are always -+
        if (ipart.typ() + jpart.typ() != 0) continue;
        if (ipart.typ() < 0)
          ret.emplace_back (i, j, ipart, jpart);
        else
          ret.emplace_back (j, i, jpart, ipart);
      }
    }
  }
  return ret;
}


class Quad
  : public FourVec
{
public:
  Quad() : m_i(0), m_j(0) {}
  Quad (size_t i, size_t j, const LPair& pa, const LPair& pb);
  int typ() const;
  float e() const { return E(); }
  float th() const { return Theta(); }
  float m4l() const { return M(); }
  float m12() const { return m_ll1->M(); }
  float m34() const { return m_ll2->M(); }
  float m14() const { return m_m14; }
  float m23() const { return m_m23; }
  float avgm() const { return (m12()+m34())/2; }
  float dm() const { return std::abs(m12()-m34()); }
  float mratio() const { return m34() / m12(); }
  
  size_t i() const { return m_i; }
  size_t j() const { return m_j; }
  const LPair& ll1() const { return *m_ll1; }
  const LPair& ll2() const { return *m_ll2; }

  float min_l_e (int typ) const;
  float min_l_pt (int typ) const;
  float max_l_aeta (int typ) const;
  float min_el_e() const { return min_l_e (11); }
  float min_mu_e() const { return min_l_e (13); }
  float min_el_pt() const { return min_l_pt (11); }
  float min_mu_pt() const { return min_l_pt (13); }
  float max_el_aeta() const { return max_l_aeta (11); }
  float max_mu_aeta() const { return max_l_aeta (13); }

  float min_sf_dr() const;
  float min_of_dr() const;

private:
  size_t m_i;
  size_t m_j;
  const LPair* m_ll1;
  const LPair* m_ll2;
  float m_m14;
  float m_m23;
};


Quad::Quad (size_t i, size_t j, const LPair& pa, const LPair& pb)
  : FourVec (pa + pb)
{
  double ma = pa.M();
  double mb = pb.M();
  if (std::abs (ma - M_Z) < std::abs (mb - M_Z)) {
    m_i = i;
    m_j = j;
    m_ll1 = &pa;
    m_ll2 = &pb;
  }
  else {
    m_i = j;
    m_j = i;
    m_ll1 = &pb;
    m_ll2 = &pa;
  }

  m_m14 = (m_ll1->ipart() + m_ll2->jpart()).M();
  m_m23 = (m_ll1->jpart() + m_ll2->ipart()).M();
}


int Quad::typ() const
{
  if (m_ll1->typ() == 11) {
    if (m_ll2->typ() == 11) return 1111;
    if (m_ll2->typ() == 13) return 1122;
  }
  if (m_ll1->typ() == 13) {
    if (m_ll2->typ() == 11) return 2211;
    if (m_ll2->typ() == 13) return 2222;
  }
  return 0;
}


float Quad::min_l_e (int typ) const
{
  double min_e = 1e6;
  if (std::abs (m_ll1->typ()) == typ) {
    min_e = std::min (min_e,
                      std::min (m_ll1->ipart().e(),
                                m_ll1->jpart().e()));
  }
  if (std::abs (m_ll2->typ()) == typ) {
    min_e = std::min (min_e,
                      std::min (m_ll2->ipart().e(),
                                m_ll2->jpart().e()));
  }
  return min_e;
}


float Quad::min_l_pt (int typ) const
{
  double min_pt = 1e6;
  if (std::abs (m_ll1->typ()) == typ) {
    min_pt = std::min (min_pt,
                      std::min (m_ll1->ipart().Pt(),
                                m_ll1->jpart().Pt()));
  }
  if (std::abs (m_ll2->typ()) == typ) {
    min_pt = std::min (min_pt,
                      std::min (m_ll2->ipart().Pt(),
                                m_ll2->jpart().Pt()));
  }
  return min_pt;
}


float Quad::max_l_aeta (int typ) const
{
  double max_aeta = -1;
  if (std::abs (m_ll1->typ()) == typ) {
    max_aeta = std::max (max_aeta,
                         std::max (std::abs (m_ll1->ipart().Eta()),
                                   std::abs (m_ll1->jpart().Eta())));
  }
  if (std::abs (m_ll2->typ()) == typ) {
    max_aeta = std::max (max_aeta,
                         std::max (std::abs (m_ll2->ipart().Eta()),
                                   std::abs (m_ll2->jpart().Eta())));
  }
  return max_aeta;
}


float Quad::min_sf_dr() const
{
  using ROOT::Math::VectorUtil::DeltaR;
  double min_dr = 100;
  min_dr = std::min (min_dr, DeltaR (ll1().ipart(), ll1().jpart()));
  min_dr = std::min (min_dr, DeltaR (ll2().ipart(), ll2().jpart()));
  if (ll1().typ() == ll2().typ()) {
    min_dr = std::min (min_dr, DeltaR (ll1().ipart(), ll2().ipart()));
    min_dr = std::min (min_dr, DeltaR (ll1().jpart(), ll2().jpart()));
    min_dr = std::min (min_dr, DeltaR (ll1().ipart(), ll2().jpart()));
    min_dr = std::min (min_dr, DeltaR (ll1().jpart(), ll2().ipart()));
  }
  return min_dr;
}


float Quad::min_of_dr() const
{
  using ROOT::Math::VectorUtil::DeltaR;
  double min_dr = 100;
  if (ll1().typ() != ll2().typ()) {
    min_dr = std::min (min_dr, DeltaR (ll1().ipart(), ll2().ipart()));
    min_dr = std::min (min_dr, DeltaR (ll1().jpart(), ll2().jpart()));
    min_dr = std::min (min_dr, DeltaR (ll1().ipart(), ll2().jpart()));
    min_dr = std::min (min_dr, DeltaR (ll1().jpart(), ll2().ipart()));
  }
  return min_dr;
}


std::vector<Quad> make_quads (const std::vector<LPair>& elepairs,
                              const std::vector<LPair>& muopairs)
{
  std::vector<Quad> ret;
  std::vector<const LPair*> pairs;
  for (const LPair& p : elepairs) pairs.push_back (&p);
  for (const LPair& p : muopairs) pairs.push_back (&p);
  size_t sz = pairs.size();
  if (sz > 1) {
    for (size_t i = 0; i < sz-1; i++) {
      for (size_t j = i+1; j < sz; j++) {
        if (pairs[i]->typ() == pairs[j]->typ()) {
          if (pairs[i]->i() == pairs[j]->i() ||
              pairs[i]->j() == pairs[j]->i()) continue;
          if (pairs[i]->i() == pairs[j]->j() ||
              pairs[i]->j() == pairs[j]->j()) continue;
        }
        ret.emplace_back (i, j, *pairs[i], *pairs[j]);
      }
    }
  }
  std::sort (ret.begin(), ret.end(),
             [] (const Quad& a, const Quad& b)
             { return std::abs(a.m12()-a.m34()) < std::abs(b.m12()-b.m34()); });
  return ret;
}


bool quadKinCut (const Quad& q)
{
  double E[] = { q.ll1().ipart().E(),
                 q.ll1().jpart().E(),
                 q.ll2().ipart().E(),
                 q.ll2().jpart().E() };
  std::sort (std::begin(E), std::end(E));
  return E[3] > 20 && E[2]> 15 && E[1] > 10;
}


bool quadKinCutPt (const Quad& q)
{
  double pt[] = { q.ll1().ipart().Pt(),
                  q.ll1().jpart().Pt(),
                  q.ll2().ipart().Pt(),
                  q.ll2().jpart().Pt() };
  std::sort (std::begin(pt), std::end(pt));
  return pt[3] > 20 && pt[2]> 15 && pt[1] > 10;
}


bool quadDRCut (const Quad& q)
{
  using ROOT::Math::VectorUtil::DeltaR2;
  const float sf_dr2_min = 0.1*0.1;
  const float of_dr2_min = 0.2*0.2;
  if (DeltaR2 (q.ll1().ipart(), q.ll1().jpart()) < sf_dr2_min) return false;
  if (DeltaR2 (q.ll2().ipart(), q.ll2().jpart()) < sf_dr2_min) return false;
  float dr2_min = sf_dr2_min;
  if (q.ll1().typ() != q.ll2().typ()) {
    dr2_min = of_dr2_min;
  }
  if (DeltaR2 (q.ll1().ipart(), q.ll2().ipart()) < dr2_min) return false;
  if (DeltaR2 (q.ll1().jpart(), q.ll2().jpart()) < dr2_min) return false;
  if (DeltaR2 (q.ll1().ipart(), q.ll2().jpart()) < dr2_min) return false;
  if (DeltaR2 (q.ll1().jpart(), q.ll2().ipart()) < dr2_min) return false;
  return true;
}


bool oniaVeto (const Quad& q, float mlo, float mhi)
{
  if (mlo < q.m12() && q.m12() < mhi) return false;
  if (mlo < q.m34() && q.m34() < mhi) return false;
  if (mlo < q.m14() && q.m14() < mhi) return false;
  if (mlo < q.m23() && q.m23() < mhi) return false;
  return true;
}


bool oniaVeto_lm (const Quad& q, float mlo, float mhi)
{
  if (mlo < q.m12() && q.m12() < mhi) return false;
  if (mlo < q.m34() && q.m34() < mhi) return false;
  return true;
}


bool oniaVeto_alt (const Quad& q, float mlo, float mhi)
{
  if (mlo < q.m14() && q.m14() < mhi) return false;
  if (mlo < q.m23() && q.m23() < mhi) return false;
  return true;
}


bool ZVeto (const Quad& q)
{
  if (q.m12() > 64) return false;
  if (q.m34() > 64) return false;
  if (q.typ() == 1111 || q.typ() == 2222) {
    if (q.m23() > 75) return false;
    if (q.m14() > 75) return false;
  }
  return true;
}


//**********************************************************************


template <class T>
class ColumnHolder
  : public TObject
{
public:
  virtual ~ColumnHolder() = default;
  void Fill (T x) { m_x = x; }
  void Merge (TList*) {}
  const T& get() const { return m_x; }
  T m_x = T();
};


namespace ROOT {
namespace Internal {
namespace RDF {

template <class T>
class FillParHelper<ColumnHolder<T> >
  :  public RActionImpl<FillParHelper<ColumnHolder<T> >>
{
  ColumnHolder<T>* fObject;
public:
  using CH = ColumnHolder<T>;
  FillParHelper(FillParHelper &&) = default;
  FillParHelper(const FillParHelper &) = delete;
  
  FillParHelper(const std::shared_ptr<CH> &h, const unsigned int /*nSlots*/)
    : fObject (h.get())
  {
  }

  void InitTask(TTreeReader *, unsigned int) {}

  void Exec(unsigned int slot, const T& x)
  {
    if (slot == 0) {
      fObject->Fill(x);
    }
  }

  void Initialize() { /* noop */}
  void Finalize() { }
  CH &PartialUpdate(unsigned int slot) { return *fObject; }
  std::string GetActionName() { return "FillPar"; }
};


}}}


template <class CH>
class CallFill
{
public:
  template <class DF>
  ROOT::RDF::RResultPtr<CH> fill (DF df, const std::string& colname)
  {
    CH obj;
    return df.Fill(std::move(obj), {colname.c_str()});
  }
};


int xprint_ev (int evevt,
               int index)
{
  std::cout << evevt << " " << index << "\n";
  return 0;
}


