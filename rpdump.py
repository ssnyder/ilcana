import ROOT
f = ROOT.TFile('tuple/ilc250_eLpR_2f1h_zdark40_500_rec.root')
t = f.MyLCTuple
t.GetEntry(0)

from math import *

def rpdump(t):
    print ('mc')
    for i in range(t.nmcp):
        #if t.mcene[i] < 5: continue
        px = t.mcmox[i]
        py = t.mcmoy[i]
        pz = t.mcmoz[i]
        pt = hypot (px, py)
        phi = atan2 (py, px)
        theta = atan2 (pz, pt)
        e = t.mcene[i]
        print (f'{t.mcpdg[i]:4} {t.mcgst[i]:2} {e:6.1f} {pt:6.1f} {theta:6.3f} {phi:6.3f}')
    print ('***')
    for i in range(t.nmcp):
        if abs(t.mcpdg[i])!=11 or t.mcgst[i]!=1: continue
        #if t.mcene[i] < 5: continue
        px = t.mcmox[i]
        py = t.mcmoy[i]
        pz = t.mcmoz[i]
        pt = hypot (px, py)
        phi = atan2 (py, px)
        theta = atan2 (pz, pt)
        e = t.mcene[i]
        print (f'{t.mcpdg[i]:4} {t.mcgst[i]:2} {e:6.1f} {pt:6.1f} {theta:6.3f} {phi:6.3f}')
    # print ('rec')
    # for i in range(t.nrec):
    #     px = t.rcmox[i]
    #     py = t.rcmoy[i]
    #     pz = t.rcmoz[i]
    #     pt = hypot (px, py)
    #     phi = atan2 (py, px)
    #     theta = atan2 (pz, pt)
    #     e = t.rcene[i]
    #     print (f'{t.rctyp[i]:4} {t.rcori[i]:2} {e:6.1f} {pt:6.1f} {theta:6.3f} {phi:6.3f} {t.rcmas[i]:6.1f}')
        
