import sys

class SampleBase:
    def __init__ (self, gdict = sys.modules['__main__'].__dict__):
        self._gdict = gdict
        self._pyname = None
        return

    def __getattr__ (self, a):
        if a.startswith ('_') and a != '__getitem__':
            if self.__dict__.has_key (a):
                return self.__dict__[a]
            raise AttributeError (a)
        if self._pyname != None:
            if not hasattr (self._gdict[self._pyname], a):
                print ('Sample', self._pyname, 'does not have attr', a)
                assert 0
            return getattr (self._gdict[self._pyname], a)
        try:
            s = self._create()
        except Exception:
            import traceback
            traceback.print_exc()
            raise
        return getattr (s, a)

    def __setattr__ (self, a, v):
        if a.startswith ('_'):
            self.__dict__[a] = v
            return
        if self._pyname != None:
            return setattr (self._gdict[self._pyname], a, v)
        s = self._create()
        return setattr (s, a, v)

