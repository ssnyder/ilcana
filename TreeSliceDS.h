// Author: Enrico Guiraud, Danilo Piparo CERN  9/2017

/*************************************************************************
 * Copyright (C) 1995-2018, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef TREESLICEDS_H
#define TREESLICEDS_H

#include "ROOT/RDataFrame.hxx"
#include "ROOT/RDataSource.hxx"
#include <TChain.h>

#include <memory>
#include <vector>
#include <unordered_map>


class TreeSliceDS : public ROOT::RDF::RDataSource {
private:
   TTree* fTree;
   ULong64_t fFirstEntry;
   ULong64_t fNEntries;
   std::vector<std::string> fListOfBranches;
   std::vector<double *> fAddressesToFree;
   std::vector<std::vector<void *>> fBranchAddresses; // first container-> slot, second -> column;
   std::vector<std::pair<ULong64_t, ULong64_t>> fEntryRanges;
   unsigned int fNSlots = 0U;
   virtual std::vector<void *> GetColumnReadersImpl(std::string_view, const std::type_info &) override;

   struct Vec
   {
     enum Typ { NONE, CHAR, INT, FLOAT, DOUBLE };
     Typ fTyp;
     union {
       ROOT::VecOps::RVec<char>* fChar;
       ROOT::VecOps::RVec<int>* fInt;
       ROOT::VecOps::RVec<float>* fFloat;
       ROOT::VecOps::RVec<double>* fDouble;
       void* fAddr;
     } fVec;
     std::string fLeafName;

     Vec (Typ typ, const std::string& leafName);
     void del();
     void set (TTree* tree);
     void* addr() { return fVec.fAddr; }
   };
   std::unordered_map<std::string, std::vector<Vec> > fVecs;


protected:
   virtual std::string AsString() override { return "TreeSliceDS"; };

public:
   TreeSliceDS(TTree* tree, ULong64_t firstEntry, ULong64_t nEntries);
   virtual ~TreeSliceDS();
   virtual std::string GetTypeName(std::string_view colName) const override;
   virtual const std::vector<std::string> &GetColumnNames() const override;
   virtual bool HasColumn(std::string_view colName) const override;
   virtual void InitSlot(unsigned int slot, ULong64_t firstEntry) override;
   virtual void FinaliseSlot(unsigned int slot) override;
   virtual std::vector<std::pair<ULong64_t, ULong64_t>> GetEntryRanges() override;
   virtual bool SetEntry(unsigned int slot, ULong64_t entry) override;
   virtual void SetNSlots(unsigned int nSlots) override;
   virtual void Initialise() override;
   virtual std::string GetLabel() override;
};



ROOT::RDataFrame MakeSliceDataFrame (TTree* tree, ULong64_t firstEntry, ULong64_t nEntries)
{
  auto sl = std::make_unique<TreeSliceDS> (tree, firstEntry, nEntries);
  return ROOT::RDataFrame (std::move (sl));
}


#endif
