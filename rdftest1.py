import ROOT
import cppyy
from draw_obj import zone, draw_obj, printpdf

# nrec
# rcori - collection id
# rccid - collection id
# rctyp - pdg id
# rccov - covariance matrix
# rcrpx, rcrpy, rcrpz - eference point?
# rcgpi - goodness of PID
# rcpiu - particle id used
# rcnpi - number of particle ids
# rcfpi - particle id coll index?
# rcmox, rcmoy, rcmoz - momentum
# rcmas - mass
# rcene - energy
# rccha - charge
# rcntr - number of tracks
# rcncl - number of clusters
# rcnrp - number of particles
# rcftr - tracks collection id
# rcvts - vertex start collection id
# rcvte - vertex end collection id
# rccom - is compound

code = open('rdftest1.cc').read()

ROOT.gInterpreter.Declare(code)

#d = ROOT.RDataFrame ('MyLCTuple', 'tuple/ilc250_eLpR_2f1h_zdark20_500_rec.root')
#d = ROOT.RDataFrame ('MyLCTuple', '../data/tuple/zd40_500_rec.root')
d = ROOT.RDataFrame ('MyLCTuple', '../data/tuple/zd40_20000_*_rec.root')
#d = ROOT.RDataFrame ('MyLCTuple', 'tuple/ilc250_eLpR_2f1h_zdark60_500_rec.root')


class Kin:
    def __init__ (self, r, lab, max_e = None, max_m = None):
        if max_e == None: max_e = 100
        if max_m == None: max_m = 100

        self.r = r
        self.vn = self.r.Define (f'{lab}_vn', f'(int){lab}.size()')
        self.n = self.vn.Histo1D (f'{lab}_vn')

        self.ve = self.r.Define (f'{lab}_ve', f'get_e({lab})')
        self.e = self.ve.Histo1D (ROOT.RDF.TH1DModel (f'{lab}_e',
                                                      f'{lab}_e',
                                                      50, 0, max_e),
                                  f'{lab}_ve')

        self.vth = self.ve.Define (f'{lab}_vth', f'get_th({lab})')
        self.th = self.vth.Histo1D (ROOT.RDF.TH1DModel (f'{lab}_th',
                                                        f'{lab}_th',
                                                        50, 0, 3.5),
                                    f'{lab}_vth')

        self.vphi = self.vth.Define (f'{lab}_vphi', f'get_phi({lab})')
        self.phi = self.vphi.Histo1D (ROOT.RDF.TH1DModel (f'{lab}_phi',
                                                          f'{lab}_phi',
                                                          50, -3.5, 3.5),
                                      f'{lab}_vphi')

        self.vm = self.vphi.Define (f'{lab}_vm', f'get_m({lab})')
        self.m = self.vm.Histo1D (ROOT.RDF.TH1DModel (f'{lab}_m',
                                                      f'{lab}_m',
                                                      50, 0, max_m),
                                  f'{lab}_vm')
        return


class Res:
    def __init__ (self, kin, lab):
        self.kin = kin
        self.vres_e = kin.vm.Define (f'v{lab}_res_e', f'get_res_e(v{lab})')
        self.res_e = self.vres_e.Histo1D (ROOT.RDF.TH1DModel ('res_e',
                                                              'res_e',
                                                              50, -0.01, 0.01),
                                          f'v{lab}_res_e')
        self.res_e_e = self.vres_e.Histo2D (ROOT.RDF.TH2DModel ('res_e',
                                                                'res_e',
                                                                25, 0, 100,
                                                                50, -0.01, 0.01),
                                            f'v{lab}_ve', f'v{lab}_res_e')
        self.res_e_th = self.vres_e.Histo2D (ROOT.RDF.TH2DModel ('res_e',
                                                                 'res_e',
                                                                 25, 0, 3.5,
                                                                 50, -0.01, 0.01),
                                             f'v{lab}_vth', f'v{lab}_res_e')
        self.res_e_phi = self.vres_e.Histo2D (ROOT.RDF.TH2DModel ('res_e',
                                                                  'res_e',
                                                                  25, -3.5, 3.5,
                                                                  50, -0.01, 0.01),
                                              f'v{lab}_vphi', f'v{lab}_res_e')



        self.vres_th = kin.vm.Define (f'v{lab}_res_th', f'get_res_th(v{lab})')
        self.res_th = self.vres_th.Histo1D (ROOT.RDF.TH1DModel ('res_th',
                                                                'res_th',
                                                                50, -0.0005, 0.0005),
                                            f'v{lab}_res_th')
        self.res_th_e = self.vres_th.Histo2D (ROOT.RDF.TH2DModel ('res_th',
                                                                  'res_th',
                                                                  25, 0, 100,
                                                                  50, -0.0005, 0.0005),
                                              f'v{lab}_ve', f'v{lab}_res_th')
        self.res_th_th = self.vres_th.Histo2D (ROOT.RDF.TH2DModel ('res_th',
                                                                   'res_th',
                                                                   25, 0, 3.5,
                                                                   50, -0.0005, 0.0005),
                                               f'v{lab}_vth', f'v{lab}_res_th')
        self.res_th_phi = self.vres_th.Histo2D (ROOT.RDF.TH2DModel ('res_th',
                                                                    'res_th',
                                                                    25, -3.5, 3.5,
                                                                    50, -0.0005, 0.0005),
                                                f'v{lab}_vphi', f'v{lab}_res_th')



        self.vres_phi = kin.vm.Define (f'v{lab}_res_phi', f'get_res_phi(v{lab})')
        self.res_phi = self.vres_phi.Histo1D (ROOT.RDF.TH1DModel ('res_phi',
                                                                  'res_phi',
                                                                  50, -0.0005, 0.0005),
                                              f'v{lab}_res_phi')
        self.res_phi_e = self.vres_phi.Histo2D (ROOT.RDF.TH2DModel ('res_phi',
                                                                    'res_phi',
                                                                    25, 0, 100,
                                                                    50, -0.0005, 0.0005),
                                                f'v{lab}_ve', f'v{lab}_res_phi')
        self.res_phi_th = self.vres_phi.Histo2D (ROOT.RDF.TH2DModel ('res_phi',
                                                                     'res_phi',
                                                                     25, 0, 3.5,
                                                                     50, -0.0005, 0.0005),
                                                 f'v{lab}_vth', f'v{lab}_res_phi')
        self.res_phi_phi = self.vres_phi.Histo2D (ROOT.RDF.TH2DModel ('res_phi',
                                                                      'res_phi',
                                                                      25, -3.5, 3.5,
                                                                      50, -0.0005, 0.0005),
                                                  f'v{lab}_vphi', f'v{lab}_res_phi')
        return


class Ana:
    def __init__ (self, rdf):
        self.rdf = rdf

        self.vmc = self.rdf.Define ('vmc', 'make_mc(mcpdg, mcgst, mcmox, mcmoy, mcmoz, mcene)')
        self.mc = Kin (self.vmc, 'vmc')

        self.defKin('mc',    'mcmuo', 'filterCont(vmc, atypStaEMCFilter(13, 1, 5))')
        self.defKin('mcmuo', 'mcele', 'filterCont(vmc, atypStaEMCFilter(11, 1, 5))')
        self.defKin('mcele', 'mcgam', 'filterCont(vmc, atypStaEMCFilter(22, 1, 5))')


        #####################################################################

        self.defKin ('mcgam', 'rps', 'make_rp(rctyp, rcori, rcmox, rcmoy, rcmoz, rcene)')
        self.defKin ('rps', 'pairs', 'make_pairs(vrps)')
        
        self.defKin ('rps', 'muo', 'filterCont(vrps, atypOriRPFilter(13, 2))')
        self.defKin ('muo', 'ele', 'filterCont(vrps, atypOriRPFilter(11, 1))')
        self.defKin ('ele', 'gam', 'filterCont(vrps, atypOriRPFilter(22, 3))')


        #####################################################################

        self.vmuomatch = self.vgam.Define ('vmuomatch', 'match(vmcmuo, vmuo)')
        self.defKin ('muomatch', 'muomatched', 'requireMatch (vmuomatch)')
        self.defKin ('muomatch', 'muounmatched', 'vetoMatch (vmuomatch)')

        self.muores = Res (self.muomatched, 'muomatched')

        self.velematch = self.vgam.Define ('velematch', 'match(vmcele, vele)')
        self.defKin ('elematch', 'elematched', 'requireMatch (velematch)')
        self.defKin ('elematch', 'eleunmatched', 'vetoMatch (velematch)')

        self.eleres = Res (self.elematched, 'elematched')

        self.vegmatch = self.vgam.Define ('vegmatch', 'match(vmcele, vgam)')
        self.defKin ('egmatch', 'egmatched', 'requireMatch (vegmatch)')

        #####################################################################

        self.valljets = self.rdf.Define ('valljets', 'make_jet(jori, jmox, jmoy, jmoz, jene)')
        self.defKin('alljets',  'j2', 'filterCont(valljets, oriJetFilter(12))')
        self.defKin('alljets',  'j3', 'filterCont(valljets, oriJetFilter(13))')
        self.defKin('alljets',  'j4', 'filterCont(valljets, oriJetFilter(14))')
        self.defKin('alljets',  'j5', 'filterCont(valljets, oriJetFilter(15))')
        self.defKin('alljets',  'j6', 'filterCont(valljets, oriJetFilter(16))')

        self.defKin ('j2', 'j2pairs', 'make_jet_pairs(vj2)', max_m = 120)

        return


    def defKin (self, frm, to, code, **kw):
        d = getattr (self, 'v'+frm).Define ('v'+to, code)
        h = Kin (d, 'v'+to, **kw)
        setattr (self, 'v'+to, d)
        setattr (self, to, h)
        return d



a = Ana(d)


def plotkin (kin, obj):
    zone (2, 2)
    kin.n.SetTitle (obj + ' N')
    draw_obj (kin.n)
    kin.e.SetTitle (obj + ' E')
    draw_obj (kin.e)
    kin.th.SetTitle (obj + ' theta')
    draw_obj (kin.th)
    kin.phi.SetTitle (obj + ' phi')
    draw_obj (kin.phi)
    return


def ploteff1 (mc, reco, obj='obj', vs='vs'):
    eff = reco.Clone (reco.GetName() + '_eff')
    eff.SetTitle (obj + ' reconstruction efficiency vs ' + vs)
    eff.Sumw2()
    mc.Sumw2()
    eff.Divide (mc.__follow__())
    draw_obj (eff)
    return eff

def ploteffs (mc, reco, obj='Muon'):
    zone (2, 2)
    ploteff1 (mc.e, reco.e, obj, 'E')
    ploteff1 (mc.th, reco.th, obj, 'theta')
    ploteff1 (mc.phi, reco.phi, obj, 'phi')
    return

def plotres1 (plots, var1, var2, title=None, max=None):
    oa = ROOT.TObjArray()
    getattr (plots, var1 + '_' + var2).FitSlicesY (cppyy.nullptr, 0, -1, 0, "", oa)
    if max != None: oa[2].SetMaximum (max)
    oa[2].SetMinimum (0)
    if title != None: oa[2].SetTitle (title)
    draw_obj (oa[2])
    return

def plotres (plots, var, varname, max=None):
    zone (2, 2)
    plotres1 (plots, 'res_' + var, 'e', varname + ' resolution vs E', max)
    plotres1 (plots, 'res_' + var, 'th', varname + ' resolution vs theta', max)
    plotres1 (plots, 'res_' + var, 'phi', varname + ' resolution vs phi', max)
    return
    

def doplots():
    plotkin  (a.ele, 'Electron')
    printpdf ('plots/elekin.pdf')
    plotkin  (a.muo, 'Muon')
    printpdf ('plots/muokin.pdf')

    ploteffs (a.mcmuo, a.muomatched, 'Muon')
    printpdf ('plots/muoeff.pdf')
    ploteffs (a.mcele, a.elematched, 'Electron')
    printpdf ('plots/eleeff.pdf')
    ploteffs (a.mcele, a.egmatched, 'Ele as gam')
    printpdf ('plots/egeff.pdf')

    plotres (a.eleres, 'e', 'Electron E', 0.03)
    printpdf ('plots/eleres_e.pdf')
    plotres (a.eleres, 'th', 'Electron theta', 0.0005)
    printpdf ('plots/eleres_th.pdf')
    plotres (a.eleres, 'phi', 'Electron phi', 0.0005)
    printpdf ('plots/eleres_phi.pdf')

    plotres  (a.muores, 'e', 'Muon E', 0.015)
    printpdf ('plots/muores_e.pdf')
    plotres  (a.muores, 'th', 'Muon theta', 0.0005)
    printpdf ('plots/muores_th.pdf')
    plotres  (a.muores, 'phi', 'Muon phi', 0.0005)
    printpdf ('plots/muores_phi.pdf')

    plotkin (a.j2, 'Jets(2)')
    printpdf ('plots/j2kin.pdf')
    plotkin (a.j6, 'Jets(6)')
    printpdf ('plots/j6kin.pdf')

    zone(1,1)
    a.j2pairs.m.SetTitle ('Jets(2) pair mass')
    draw_obj(a.j2pairs.m)
    printpdf ('plots/j2pair.pdf')

    return

zone(1,1)
