//
// Interface for coordinate class C:
//   using Scalar = ...;
//   C();
//   C(const Scalar&, const Scalar&, const Scalar&, const Scalar&);
//   template <...> C(const COORD&);
//   template <...> C& operator= (const COORD&);
//   void SetCoordinates (const Scalar src[]);
//   void SetCoordinates (Scalar, Scalar, Scalar, Scalar);
//   void GetCoordinates (Scalar&, Scalar&, Scalar&, Scalar&) const;
//   void GetCoordinates (Scalar dest[]) const;
//   void SetPxPyPzE (Scalar, Scalar, Scalar, Scalar);
//   bool operator== (const C&);
//   Scalar Px() const;
//   Scalar Py() const;
//   Scalar Pz() const;
//   Scalar E() const;
//   Scalar M2() const;
//   Scalar M() const;
//   Scalar R() const;
//   Scalar Perp2() const;
//   Scalar Pt() const;
//   Scalar Mt2() const;
//   Scalar Mt() const;
//   Scalar Et2() const;
//   Scalar Et() const;
//   Scalar Phi() const;
//   Scalar Theta() const;
//   Scalar Eta() const;
//   Scalar Rho() const;
//   void Scale (Scalar);
//   void SetE (Scalar);
//   void SetEta (Scalar);
//   void SetM (Scalar);
//   void SetPhi (Scalar);
//   void SetPt (Scalar);
//   void SetPx (Scalar);
//   void SetPy (Scalar);
//   void SetPz (Scalar);

// Need to override coord type::
//   template <...> LorentzVector operator+ (const OTHER&) const;
//   template <...> LorentzVector operator- (const OTHER&) const;
//   template <...> LorentzVector operator* (const OTHER&) const;
//   template <...> LorentzVector operator/ (const OTHER&) const;
//   template <...> LorentzVector operator- (const OTHER&) const;
//   free operator*

#include "MyLorentzVector.h"
#include "PxPyPzE4DSOA.h"

using FourVec = ROOT::Math::PxPyPzMVector;
using namespace ROOT::VecOps;


using FourVecSOA = MyMath::LorentzVector<MyMath::PxPyPzE4DSOA<float> >;


//*************************************************************************


struct ParticleStruct
  : public MyMath::PxPyPzE4DSOAStruct<float>
{
  ParticleStruct() {}
  ParticleStruct (const int* typ,
                  const float* mox,
                  const float* moy,
                  const float* moz,
                  const float* ene)
    : MyMath::PxPyPzE4DSOAStruct<float> (mox, moy, moz, ene),
      m_typ (typ)
  {
  }


  int typ (size_t ndx) const { return m_typ[ndx]; }

private:
  const int* m_typ = nullptr;
};


class Particle
  : public FourVecSOA
{
public:
  using base = FourVecSOA;
  using base::base;

  Particle() = default;
  Particle (size_t ndx, const ParticleStruct& soa)
    : base (MyMath::PxPyPzE4DSOA<float> (ndx, soa))
  { }

  int typ() const { return arrs().typ (ndx()); }

  size_t ndx() const { return Coordinates().ndx(); }

  bool valid() const { return Coordinates().valid(); }


private:
  const ParticleStruct& arrs() const
  {
    return static_cast<const ParticleStruct&> (Coordinates().arrs());
  }
};


//*************************************************************************


struct RecoPartStruct
  : public ParticleStruct
{
  RecoPartStruct() {}
  RecoPartStruct (const int* typ,
                  const int* ori,
                  const float* mox,
                  const float* moy,
                  const float* moz,
                  const float* ene)
    : ParticleStruct (typ, mox, moy, moz, ene),
      m_ori (ori)
  {
  }


  int ori (size_t ndx) const { return m_ori[ndx]; }

private:
  const int* m_ori = nullptr;
};


class RecoPart
  : public Particle
{
public:
  using base = Particle;
  using base::base;

  RecoPart() = default;
  RecoPart (size_t ndx, const RecoPartStruct& soa)
    : Particle (MyMath::PxPyPzE4DSOA<float> (ndx, soa))
  { }

  int ori() const { return arrs().ori (ndx()); }

  size_t ndx() const { return Coordinates().ndx(); }

  bool valid() const { return Coordinates().valid(); }


private:
  const RecoPartStruct& arrs() const
  {
    return static_cast<const RecoPartStruct&> (Coordinates().arrs());
  }
};


class RecoPartContainer
{
public:
  using value_type = RecoPart;

  RecoPartContainer() {}
  
  RecoPartContainer (const RVec<int>& typ,
                     const RVec<int>& ori,
                     const RVec<float>& mox,
                     const RVec<float>& moy,
                     const RVec<float>& moz,
                     const RVec<float>& ene)
    : m_arrs (typ.data(), ori.data(),
              mox.data(), moy.data(), moz.data(), ene.data()),
      m_size (mox.size())
  {
  }

  size_t size() const { return m_size; }
  
  RecoPart operator[] (size_t ndx) const
  { return RecoPart (ndx, m_arrs); }


  class const_iterator
  {
  public:
    const_iterator (size_t ndx, const RecoPartStruct& arrs)
      : m_ndx(ndx), m_arrs (arrs)
    {
    }


    const_iterator& operator++()
    {
      ++m_ndx;
      return *this;
    }

    RecoPart operator*() const
    {
      return RecoPart (m_ndx, m_arrs);
    }

    bool operator== (const const_iterator& other) const
    {
      return m_ndx == other.m_ndx && &m_arrs == &other.m_arrs;
    }
    bool operator!= (const const_iterator& other) const
    {
      return !(*this == other);
    }


  private:
    size_t m_ndx;
    const RecoPartStruct& m_arrs;
  };


  const_iterator begin() const
  {
    return const_iterator (0, m_arrs);
  }

  const_iterator end() const
  {
    return const_iterator (m_size, m_arrs);
  }

  
private:
  RecoPartStruct m_arrs;
  size_t m_size = 0;
};


RecoPartContainer make_rp (const RVec<int>& typ,
                           const RVec<int>& ori,
                           const RVec<float>& mox,
                           const RVec<float>& moy,
                           const RVec<float>& moz,
                           const RVec<float>& ene)
{
  return RecoPartContainer (typ, ori, mox, moy, moz, ene);
}


//*************************************************************************


struct MCPartStruct
  : public ParticleStruct
{
  MCPartStruct() {}
  MCPartStruct (const int* typ,
                const int* sta,
                const float* mox,
                const float* moy,
                const float* moz,
                const float* ene)
    : ParticleStruct (typ, mox, moy, moz, ene),
      m_sta (sta)
  {
  }


  int sta (size_t ndx) const { return m_sta[ndx]; }

private:
  const int* m_sta = nullptr;
};


class MCPart
  : public Particle
{
public:
  using base = Particle;
  using base::base;

  MCPart (size_t ndx, const MCPartStruct& soa)
    : base (MyMath::PxPyPzE4DSOA<float> (ndx, soa))
  { }

  int sta() const { return arrs().sta (ndx()); }

  size_t ndx() const { return Coordinates().ndx(); }


private:
  const MCPartStruct& arrs() const
  {
    return static_cast<const MCPartStruct&> (Coordinates().arrs());
  }
};


class MCPartContainer
{
public:
  using value_type = MCPart;

  MCPartContainer() {}
  
  MCPartContainer (const RVec<int>& typ,
                   const RVec<int>& sta,
                   const RVec<float>& mox,
                   const RVec<float>& moy,
                   const RVec<float>& moz,
                   const RVec<float>& ene)
    : m_arrs (typ.data(), sta.data(),
              mox.data(), moy.data(), moz.data(), ene.data()),
      m_size (mox.size())
  {
  }

  size_t size() const { return m_size; }
  
  MCPart operator[] (size_t ndx) const
  { return MCPart (ndx, m_arrs); }


  class const_iterator
  {
  public:
    const_iterator (size_t ndx, const MCPartStruct& arrs)
      : m_ndx(ndx), m_arrs (arrs)
    {
    }


    const_iterator& operator++()
    {
      ++m_ndx;
      return *this;
    }

    MCPart operator*() const
    {
      return MCPart (m_ndx, m_arrs);
    }

    bool operator== (const const_iterator& other) const
    {
      return m_ndx == other.m_ndx && &m_arrs == &other.m_arrs;
    }
    bool operator!= (const const_iterator& other) const
    {
      return !(*this == other);
    }


  private:
    size_t m_ndx;
    const MCPartStruct& m_arrs;
  };


  const_iterator begin() const
  {
    return const_iterator (0, m_arrs);
  }

  const_iterator end() const
  {
    return const_iterator (m_size, m_arrs);
  }

  
private:
  MCPartStruct m_arrs;
  size_t m_size = 0;
};


MCPartContainer make_mc (const RVec<int>& typ,
                         const RVec<int>& sta,
                         const RVec<float>& mox,
                         const RVec<float>& moy,
                         const RVec<float>& moz,
                         const RVec<float>& ene,
                         int evevt)
{
  return MCPartContainer (typ, sta, mox, moy, moz, ene);
}


//*************************************************************************


class MatchedPart
  : public MCPart
{
public:
  MatchedPart (const MCPart& mc)
    : MCPart (mc)
  {
  }
  MatchedPart (const MCPart& mc, const RecoPart& rp)
    : MCPart (mc), m_rp (rp)
  {
  }


  bool matched() const { return m_rp.valid(); }

  const RecoPart& rp() const { return m_rp; }

  
private:
  RecoPart m_rp;
};


//*************************************************************************


struct JetPartStruct
  : public MyMath::PxPyPzE4DSOAStruct<float>
{
  JetPartStruct() {}
  JetPartStruct (const int* ori,
                 const float* mox,
                 const float* moy,
                 const float* moz,
                 const float* ene)
    : MyMath::PxPyPzE4DSOAStruct<float> (mox, moy, moz, ene),
      m_ori (ori)
  {
  }


  int ori (size_t ndx) const { return m_ori[ndx]; }

private:
  const int* m_ori = nullptr;
};


class JetPart
  : public MyMath::LorentzVector<MyMath::PxPyPzE4DSOA<float> >
{
public:
  using base = MyMath::LorentzVector<MyMath::PxPyPzE4DSOA<float> >;
  using base::base;

  JetPart() = default;
  JetPart (size_t ndx, const JetPartStruct& soa)
    : MyMath::LorentzVector<MyMath::PxPyPzE4DSOA<float> > (MyMath::PxPyPzE4DSOA<float> (ndx, soa))
  { }

  int ori() const { return arrs().ori (ndx()); }

  size_t ndx() const { return Coordinates().ndx(); }

  bool valid() const { return Coordinates().valid(); }


private:
  const JetPartStruct& arrs() const
  {
    return static_cast<const JetPartStruct&> (Coordinates().arrs());
  }
};


class JetPartContainer
{
public:
  using value_type = JetPart;

  JetPartContainer() {}
  
  JetPartContainer (const RVec<int>& ori,
                    const RVec<float>& mox,
                    const RVec<float>& moy,
                    const RVec<float>& moz,
                    const RVec<float>& ene)
    : m_arrs (ori.data(),
              mox.data(), moy.data(), moz.data(), ene.data()),
      m_size (mox.size())
  {
  }

  size_t size() const { return m_size; }
  
  JetPart operator[] (size_t ndx) const
  { return JetPart (ndx, m_arrs); }


  class const_iterator
  {
  public:
    const_iterator (size_t ndx, const JetPartStruct& arrs)
      : m_ndx(ndx), m_arrs (arrs)
    {
    }


    const_iterator& operator++()
    {
      ++m_ndx;
      return *this;
    }

    JetPart operator*() const
    {
      return JetPart (m_ndx, m_arrs);
    }

    bool operator== (const const_iterator& other) const
    {
      return m_ndx == other.m_ndx && &m_arrs == &other.m_arrs;
    }
    bool operator!= (const const_iterator& other) const
    {
      return !(*this == other);
    }


  private:
    size_t m_ndx;
    const JetPartStruct& m_arrs;
  };

  using iterator = const_iterator;


  const_iterator begin() const
  {
    return const_iterator (0, m_arrs);
  }

  const_iterator end() const
  {
    return const_iterator (m_size, m_arrs);
  }

  
private:
  JetPartStruct m_arrs;
  size_t m_size = 0;
};


JetPartContainer make_jet (const RVec<int>& ori,
                           const RVec<float>& mox,
                           const RVec<float>& moy,
                           const RVec<float>& moz,
                           const RVec<float>& ene)
{
  return JetPartContainer (ori, mox, moy, moz, ene);
}





//*************************************************************************



template <class F, class CONT>
RVec<float> contToRVec (const CONT& cont, F f)
{
  size_t sz = cont.size();
  RVec<float> ret (sz);
  for (size_t i = 0; i < sz; i++) {
    ret[i] = f (cont[i]);
  }
  return ret;
}
  

template <class CONT>
RVec<float> get_e (const CONT& rps)
{
  return contToRVec (rps, [](const auto& rp) { return rp.E(); });
}


template <class CONT>
RVec<float> get_th (const CONT& rps)
{
  return contToRVec (rps, [](const auto& rp) { return rp.Theta(); });
}


template <class CONT>
RVec<float> get_phi (const CONT& rps)
{
  return contToRVec (rps, [](const auto& rp) { return rp.Phi(); });
}


template <class CONT>
RVec<float> get_m (const CONT& rps)
{
  return contToRVec (rps, [](const auto& rp) { return rp.M(); });
}


template <class CONT>
RVec<float> get_res_e (const CONT& mps)
{
  return contToRVec (mps, [](const auto& mp) -> float {
    float e_mc = mp.E();
    if (e_mc == 0) return 0;
    float e_rp = mp.rp().e();
    return (e_rp - e_mc) / e_mc;
  });
}


template <class CONT>
RVec<float> get_res_th (const CONT& mps)
{
  return contToRVec (mps, [](const auto& mp) -> float {
    return mp.rp().Theta() - mp.Theta();
  });
}


template <class CONT>
RVec<float> get_res_phi (const CONT& mps)
{
  return contToRVec (mps, [](const auto& mp) -> float {
    return mp.rp().Phi() - mp.Phi();
  });
}


template <class CONT1, class CONT2>
RVec<float> get_min_dr (const CONT1& c1, const CONT2& c2)
{
  return contToRVec (c1, [&](const auto& p) -> float {
      float mindr2 = 5*5;
      for (const auto& p2 : c2) {
        float dr2 = ROOT::Math::VectorUtil::DeltaR2 (p, p2);
        if (dr2 < mindr2) {
          mindr2 = dr2;
        }
      }
      return sqrt(mindr2);
    });
}


RVec<FourVec> make_pairs (const RecoPartContainer& rps)
{
  RVec<FourVec> ret;
  size_t sz = rps.size();
  if (sz > 0) {
    for (size_t i = 0; i < sz-1; ++i) {
      RecoPart ipart = rps[i];
      if (std::abs(ipart.typ()) != 11 && std::abs(ipart.typ()) != 13) continue;
      if (ipart.ori() != 1 && ipart.ori() != 2) continue;
      for (size_t j = i+1; j < sz; ++j) {
        RecoPart jpart = rps[j];
        if (ipart.typ() + jpart.typ() != 0) continue;
        if (jpart.ori() != 1 && jpart.ori() != 2) continue;
        ret.emplace_back (ipart+jpart);
        //for (size_t k = 0; k < sz; k++) {
        //  if (typ[k] == 22) {
        //    FourVec c (mox[k], moy[k], moz[k], mas[k]);
        //    if (c.E() > 5) {
        //      ret.emplace_back (a+b+c);
        //    }
        //  }
        //}
      }
    }
  }
  return ret;
}

RVec<float> getM (const RVec<FourVec>& v)
{
  return Map (v, [](const FourVec& fv) { return fv.M(); });
}


auto atypOriRPFilter (int typ, int ori)
{
  return [typ, ori](const RecoPart& rp) { return rp.ori() == ori && abs(rp.typ()) == typ; };
}

auto atypStaEMCFilter (int typ, int sta, float e)
{
  return [typ, sta, e](const MCPart& mc) { return mc.sta() == sta && abs(mc.typ()) == typ && mc.E() > e; };
}

auto atypStaEThetaMCFilter (int typ, int sta, float e, float th)
{
  return [typ, sta, e, th](const MCPart& mc) {
    return mc.sta() == sta && abs(mc.typ()) == typ && mc.E() > e &&
      mc.theta() > th && mc.theta() < (M_PI - th);
  };
}

auto atypStaPtMCFilter (int typ, int sta, float pt)
{
  return [typ, sta, pt](const MCPart& mc) { return mc.sta() == sta && abs(mc.typ()) == typ && mc.Pt() > pt; };
}

auto oriJetFilter (int ori)
{
  return [ori](const JetPart& jp) { return jp.ori() == ori; };
}

auto EFilter (float ethresh)
{
  return [ethresh](const RecoPart& rp) { return rp.E() >= ethresh; };
}

auto EThetaFilter (float ethresh, float ththresh)
{
  return [ethresh, ththresh](const RecoPart& rp) {
    return rp.E() >= ethresh && rp.theta() > ththresh && rp.theta() < (M_PI - ththresh); };
}

template <class CONT, class F>
std::vector<typename CONT::value_type> filterCont (const CONT& cont, F f)
{
  std::vector<typename CONT::value_type> ret;
  size_t sz = cont.size();
  ret.reserve (sz);
  for (const auto& rp : cont) {
    if (f (rp)){
      ret.push_back (rp);
    }
  }
  return ret;
}


template <class RP_CONT>
RecoPart matchOne (const MCPart& mc, const RP_CONT& rps)
{
  RecoPart p;
  float mindr2 = 0.1*0.1;
  for (const RecoPart rp : rps) {
    float dr2 = ROOT::Math::VectorUtil::DeltaR2 (mc, rp);
    if (dr2 < mindr2) {
      mindr2 = dr2;
      p = rp;
    }
  }
  return p;
}


template <class MC_CONT, class RP_CONT>
std::vector<MatchedPart>
match (const MC_CONT& mcs, const RP_CONT& rps)
{
  std::vector<MatchedPart> ret;
  ret.reserve (mcs.size());
  for (const MCPart mc : mcs) {
    ret.emplace_back (mc, matchOne (mc, rps));
  }
  return ret;
}


template <class CONT>
std::vector<MatchedPart>
requireMatch (const CONT& cont)
{
  std::vector<MatchedPart> ret;
  ret.reserve (cont.size());
  for (const MatchedPart mp : cont) {
    if (mp.matched()) {
      ret.push_back (mp);
    }
  }
  return ret;
}


template <class CONT>
std::vector<MatchedPart>
vetoMatch (const CONT& cont)
{
  std::vector<MatchedPart> ret;
  ret.reserve (cont.size());
  for (const MatchedPart mp : cont) {
    if (!mp.matched()) {
      ret.push_back (mp);
    }
  }
  return ret;
}


RVec<FourVec> make_jet_pairs (const std::vector<JetPart>& jps)
{
  RVec<FourVec> ret;
  size_t sz = jps.size();
  if (sz > 0) {
    for (size_t i = 0; i < sz-1; ++i) {
      JetPart ipart = jps[i];
      for (size_t j = i+1; j < sz; ++j) {
        JetPart jpart = jps[j];
        ret.emplace_back (ipart+jpart);
      }
    }
  }
  return ret;
}


//*************************************************************************

int print_ev (int evrun,
              int evevt,
              int index,
              int findex,
              int iFile,
              int proc,
              int typ)
{
  std::cout  << "runevt | " << index << " | " << findex << " | " << iFile << " | " << evrun << " | " << evevt << " | " << proc << " | " << typ << "\n";
  return 0;
}
