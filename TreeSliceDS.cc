#include "TreeSliceDS.h"
#include <ROOT/RDF/Utils.hxx>
#include <ROOT/TSeq.hxx>
#include <TClass.h>
#include <TError.h>

#include <algorithm>
#include <vector>


TreeSliceDS::Vec::Vec (Typ typ, const std::string& leafName)
  : fTyp (typ),
    fLeafName (leafName)
{
  switch (typ) {
  case CHAR:
    fVec.fChar = new ROOT::VecOps::RVec<char>;
    break;
  case INT:
    fVec.fInt = new ROOT::VecOps::RVec<int>;
    break;
  case FLOAT:
    fVec.fFloat = new ROOT::VecOps::RVec<float>;
    break;
  case DOUBLE:
    fVec.fDouble = new ROOT::VecOps::RVec<double>;
    break;
  case NONE:
    fprintf (stderr, "ERROR: Got NONE\n");
    break;
  }
}

void TreeSliceDS::Vec::del()
{
  switch (fTyp) {
  case CHAR:
    delete fVec.fChar;
    break;
  case INT:
    delete fVec.fInt;
    break;
  case FLOAT:
    delete fVec.fFloat;
    break;
  case DOUBLE:
    delete fVec.fDouble;
    break;
  case NONE:
    fprintf (stderr, "ERROR: Got NONE\n");
    break;
  }
}


void TreeSliceDS::Vec::set (TTree* tree)
{
  TLeaf* leaf = tree->GetLeaf (fLeafName.c_str());
  switch (fTyp) {
  case CHAR:
    {
      ROOT::VecOps::RVec<char> v ((char*)leaf->GetValuePointer(), leaf->GetLen());
      fVec.fChar->swap (v);
    }
    break;
  case INT:
    {
      ROOT::VecOps::RVec<int> v ((int*)leaf->GetValuePointer(), leaf->GetLen());
      fVec.fInt->swap (v);
    }
    break;
  case FLOAT:
    {
      ROOT::VecOps::RVec<float> v ((float*)leaf->GetValuePointer(), leaf->GetLen());
      fVec.fFloat->swap (v);
    }
    break;
  case DOUBLE:
    {
      ROOT::VecOps::RVec<double> v ((double*)leaf->GetValuePointer(), leaf->GetLen());
      fVec.fDouble->swap (v);
    }
    break;
  case NONE:
    fprintf (stderr, "ERROR: Got NONE\n");
    break;
  }
}


std::vector<void *> TreeSliceDS::GetColumnReadersImpl(std::string_view name, const std::type_info &id)
{
   const auto colTypeName = GetTypeName(name);
   const auto &colTypeId = ROOT::Internal::RDF::TypeName2TypeID(colTypeName);
   if (id != colTypeId) {
      std::string err = "The type of column \"";
      err += name;
      err += "\" is ";
      err += colTypeName;
      err += " but a different one has been selected.";
      throw std::runtime_error(err);
   }

   const auto index =
      std::distance(fListOfBranches.begin(), std::find(fListOfBranches.begin(), fListOfBranches.end(), name));
   std::vector<void *> ret(fNSlots);
   for (auto slot : ROOT::TSeqU(fNSlots)) {
      ret[slot] = (void *)&fBranchAddresses[index][slot];
   }
   return ret;
}

TreeSliceDS::TreeSliceDS(TTree* tree, ULong64_t firstEntry, ULong64_t nEntries)
  : fTree (tree),
    fFirstEntry (firstEntry),
    fNEntries (nEntries)
{
   const TObjArray &lob = *fTree->GetListOfBranches();
   fListOfBranches.reserve(lob.GetEntries());

   TIterCategory<TObjArray> iter(&lob);
   for (auto it = iter.Begin(); it != iter.End(); ++it) {
     TBranch* br = dynamic_cast<TBranch*>(*it);
     TLeaf* l = fTree->GetLeaf (br->GetName());
     // Skip these for now... RDF gets confused by them.
     if (l && l->GetLeafCount() && l->GetLenStatic() > 1) continue;
     fListOfBranches.push_back (br->GetName());
   }
}

TreeSliceDS::~TreeSliceDS()
{
   for (auto addr : fAddressesToFree) {
      delete addr;
   }
   for (auto& p : fVecs) {
     for (Vec& vv : p.second) {
       vv.del();
     }
   }
}

std::string TreeSliceDS::GetTypeName(std::string_view colName) const
{
   if (!HasColumn(colName)) {
      std::string e = "The dataset does not have column ";
      e += colName;
      throw std::runtime_error(e);
   }
   auto typeName = ROOT::Internal::RDF::ColumnName2ColumnTypeName(std::string(colName), fTree, /*ds=*/nullptr,
                                                                  /*customCol=*/nullptr);
   // We may not have yet loaded the library where the dictionary of this type is
   TClass::GetClass(typeName.c_str());
   return typeName;
}

const std::vector<std::string> &TreeSliceDS::GetColumnNames() const
{
   return fListOfBranches;
}

bool TreeSliceDS::HasColumn(std::string_view colName) const
{
   if (!fListOfBranches.empty())
      GetColumnNames();
   return fListOfBranches.end() != std::find(fListOfBranches.begin(), fListOfBranches.end(), colName);
}

void TreeSliceDS::InitSlot(unsigned int slot, ULong64_t firstEntry)
{
   if (slot >= 1) return;
   fTree->GetEntry (fFirstEntry + firstEntry);
   for (auto i : ROOT::TSeqU(fListOfBranches.size())) {
      auto colName = fListOfBranches[i].c_str();
      auto &addr = fBranchAddresses[i][slot];
      auto typeName = GetTypeName(colName);
      if (strncmp (typeName.c_str(), "ROOT::VecOps::RVec<", 19) == 0) {
        auto it = fVecs.find (fListOfBranches[i]);
        if (it == fVecs.end()) {
          printf ("ERROR: Can't find branch %s\n", fListOfBranches[i].c_str());
          continue;
        }
        if (it->second.size() <= slot) {
          printf ("ERROR: missing slot %s %d %d\n",
                  fListOfBranches[i].c_str(), (int)it->second.size(), (int)slot);
          continue;
        }
        Vec& v = it->second[slot];
        addr = v.addr();
        continue;
      }
      auto typeClass = TClass::GetClass(typeName.c_str());
      if (typeClass) {
         fTree->SetBranchAddress(colName, &addr, nullptr, typeClass, EDataType(0), true);
      } else {
         if (!addr) {
            addr = new double();
            fAddressesToFree.emplace_back((double *)addr);
         }
         fTree->SetBranchAddress(colName, addr);
      }
   }
}

void TreeSliceDS::FinaliseSlot(unsigned int slot)
{
}

std::vector<std::pair<ULong64_t, ULong64_t>> TreeSliceDS::GetEntryRanges()
{
   auto entryRanges(std::move(fEntryRanges)); // empty fEntryRanges
   return entryRanges;
}

bool TreeSliceDS::SetEntry(unsigned int slot, ULong64_t entry)
{
   if (slot == 0) {
     fTree->GetEntry(entry + fFirstEntry);

     for (auto& p : fVecs) {
       p.second[slot].set (fTree);
     }
   }
   return true;
}

void TreeSliceDS::SetNSlots(unsigned int nSlots)
{
   R__ASSERT(0U == fNSlots && "Setting the number of slots even if the number of slots is different from zero.");

   fNSlots = nSlots;

   const auto nColumns = fListOfBranches.size();
   // Initialise the entire set of addresses
   fBranchAddresses.resize(nColumns, std::vector<void *>(fNSlots, nullptr));

   for (const std::string& brname : fListOfBranches) {
     std::string typeName = GetTypeName (brname);
     Vec::Typ typ = Vec::NONE;
     if (typeName == "ROOT::VecOps::RVec<Char_t>") {
       typ = Vec::CHAR;
     }
     else if (typeName == "ROOT::VecOps::RVec<Int_t>") {
       typ = Vec::INT;
     }
     else if (typeName == "ROOT::VecOps::RVec<Float_t>") {
       typ = Vec::FLOAT;
     }
     else if (typeName == "ROOT::VecOps::RVec<Double_t>") {
       typ = Vec::DOUBLE;
     }
     else if (strncmp (typeName.c_str(), "ROOT::VecOps::RVec<", 19) == 0) {
       fprintf (stderr, "ERROR: Unhandled RVec %s\n", typeName.c_str());
     }

     if (typ != Vec::NONE) {
       TLeaf* leaf = fTree->GetLeaf (brname.c_str());
       if (!leaf) {
         fprintf (stderr, "ERROR: Cannot find leaf %s\n", brname.c_str());
       }
       else {
         std::vector<Vec>& v = fVecs[brname];
         v.reserve (nSlots);
         for (unsigned int i=0; i < nSlots; i++) {
           v.emplace_back (typ, leaf->GetName());
         }
       }
     }
   }
}

void TreeSliceDS::Initialise()
{
   ULong64_t nentries = fNEntries;
   if (fTree->GetEntries() <= fFirstEntry) {
     nentries = 0;
   }
   else if (fTree->GetEntries() - fFirstEntry < nentries) {
     nentries = fTree->GetEntries() - fFirstEntry;
   }
   const auto chunkSize = nentries;
   auto start = 0UL;
   auto end = 0UL;
   for (auto i : ROOT::TSeqU(fNSlots)) {
      if (i == 0)
        fEntryRanges.emplace_back(0, nentries);
      else
        fEntryRanges.emplace_back(0, 0);
   }
}

std::string TreeSliceDS::GetLabel()
{
   return "TreeSlice";
}

