#ifndef PXPYPZE4DSOA_H
#define PXPYPZE4DSOA_H

#include "Math/GenVector/eta.h"
#include "Math/GenVector/GenVector_exception.h"


#include <cmath>


class SOAEltBase
{
public:
  SOAEltBase() = default;
  SOAEltBase (size_t ndx, const void* arrs)
    : m_ndx (ndx), m_arrs (arrs)
  { }

  bool valid() const { return m_arrs != nullptr; }

  size_t ndx() const { return m_ndx; }
  const void* arrs() const { return m_arrs; }
    
private:
  size_t m_ndx = 0;
  const void* m_arrs = nullptr;
};


namespace MyMath {


template <class ScalarType = double>
class PxPyPzE4DSOAStruct
{
public:
  PxPyPzE4DSOAStruct() {}
  PxPyPzE4DSOAStruct (const ScalarType* px,
                      const ScalarType* py,
                      const ScalarType* pz,
                      const ScalarType* e)
    : m_px(px), m_py(py), m_pz(pz), m_e(e) { }


  ScalarType Px (size_t ndx) const { return m_px[ndx]; }
  ScalarType Py (size_t ndx) const { return m_py[ndx]; }
  ScalarType Pz (size_t ndx) const { return m_pz[ndx]; }
  ScalarType E (size_t ndx) const { return m_e[ndx]; }

private:
  const ScalarType* m_px = nullptr;
  const ScalarType* m_py = nullptr;
  const ScalarType* m_pz = nullptr;
  const ScalarType* m_e = nullptr;
};


template <class ScalarType = double>
class PxPyPzE4DSOA
  : public SOAEltBase
{

public :

   typedef ScalarType Scalar;

   // --------- Constructors ---------------

    PxPyPzE4DSOA() = default;
    PxPyPzE4DSOA(size_t ndx, const PxPyPzE4DSOAStruct<ScalarType>& soa)
      : SOAEltBase (ndx, &soa) { }


   /**
      get internal data into an array of 4 Scalar numbers
   */
   void GetCoordinates( Scalar dest[] ) const
   { dest[0] = Px(); dest[1] = Py(); dest[2] = Px(); dest[3] = E(); }

   /**
      get internal data into 4 Scalar numbers
   */
   void GetCoordinates(Scalar& px, Scalar& py, Scalar& pz, Scalar& e) const
   { px=Px(); py=Py(); pz=Px(); e=E();}

   // --------- Coordinates and Coordinate-like Scalar properties -------------

   // cartesian (Minkowski)coordinate accessors

   Scalar Px() const { return arrs().Px (ndx()); }
   Scalar Py() const { return arrs().Py (ndx()); }
   Scalar Pz() const { return arrs().Pz (ndx()); }
   Scalar E()  const { return arrs().E (ndx()); }

   Scalar x() const { return Px(); }
   Scalar y() const { return Py(); }
   Scalar z() const { return Pz(); }
   Scalar t() const { return E(); }

   /**
      squared magnitude of spatial components
   */
   Scalar P2() const { return Px()*Px() + Py()*Py() + Pz()*Pz(); }

   /**
      magnitude of spatial components (magnitude of 3-momentum)
   */
   Scalar P() const { return sqrt(P2()); }
   Scalar R() const { return P(); }

   /**
      vector magnitude squared (or mass squared)
   */
   Scalar M2() const   { return E()*E() - Px()*Px() - Py()*Py() - Pz()*Pz();}
   Scalar Mag2() const { return M2(); }

   /**
      invariant mass
   */
   Scalar M() const
   {
      const Scalar mm = M2();
      if (mm >= 0) {
         return sqrt(mm);
      } else {
          ROOT::Math::GenVector::Throw ("PxPyPzE4DSOA::M() - Tachyonic:\n"
                   "    P^2 > E^2 so the mass would be imaginary");
         return -sqrt(-mm);
      }
   }
   Scalar Mag() const    { return M(); }

   /**
       transverse spatial component squared
   */
   Scalar Pt2()   const { return Px()*Px() + Py()*Py();}
   Scalar Perp2() const { return Pt2();}

   /**
      Transverse spatial component (P_perp or rho)
   */
   Scalar Pt() const { return sqrt(Perp2()); }
   Scalar Perp() const { return Pt();}
   Scalar Rho()  const { return Pt();}

   /**
       transverse mass squared
   */
   Scalar Mt2() const { return E()*E() - Pz()*Pz(); }

   /**
      transverse mass
   */
   Scalar Mt() const {
      const Scalar mm = Mt2();
      if (mm >= 0) {
         return sqrt(mm);
      } else {
         ROOT::Math::GenVector::Throw ("PxPyPzE4D::Mt() - Tachyonic:\n"
                           "    Pz^2 > E^2 so the transverse mass would be imaginary");
         return -sqrt(-mm);
      }
   }

   /**
       transverse energy squared
   */
   Scalar Et2() const {  // is (E^2 * pt ^2) / p^2
      // but it is faster to form p^2 from pt^2
      Scalar pt2 = Pt2();
      return pt2 == 0 ? 0 : E()*E() * pt2/( pt2 + Pz()*Pz() );
   }

   /**
      transverse energy
   */
   Scalar Et() const {
      const Scalar etet = Et2();
      return E() < 0.0 ? -sqrt(etet) : sqrt(etet);
   }

   /**
      azimuthal angle
   */
   Scalar Phi() const { return (Px() == 0.0 && Py() == 0.0) ? 0 : atan2(Py(), Px()); }

   /**
      polar angle
   */
   Scalar Theta() const { return (Px() == 0.0 && Py() == 0.0 && Pz() == 0.0) ? 0 : atan2(Pt(), Pz()); }

   /**
       pseudorapidity
   */
   Scalar Eta() const {
      return ROOT::Math::Impl::Eta_FromRhoZ ( Pt(), Pz());
   }

   /**
      Exact equality
   */
   bool operator == (const PxPyPzE4DSOA & rhs) const {
      return Px() == rhs.Px() && Py() == rhs.Py() && Pz() == rhs.Pz() && E() == rhs.E();
   }
   bool operator != (const PxPyPzE4DSOA & rhs) const {return !(operator==(rhs));}


  //protected:
  const PxPyPzE4DSOAStruct<ScalarType>& arrs() const
  {
    return *static_cast<const PxPyPzE4DSOAStruct<ScalarType>*> (SOAEltBase::arrs());
  }
};


template <class ScalarType>
struct CoordSystemTraits<PxPyPzE4DSOA<ScalarType> >
{
  using WritableCoordSystem = ::ROOT::Math::PxPyPzE4D<ScalarType>;
};


} // end namespace MyMath


#endif // PXPYPZE4DSOA_H
