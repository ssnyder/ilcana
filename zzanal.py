#!/usr/bin/env python

import sys
from pyLCIO.drivers.Driver import Driver
from ROOT import EVENT, IOIMPL
from PDG import pdgid_to_name
import PDG

ZDD = 0
ZUU = 1
ZSS = 2
ZCC = 3
ZBB = 4
ZNUENUE = 5
ZNUMNUM = 6
ZNUTNUT = 7
ZEE = 8
ZMM = 9
ZTT = 10
ZBAD = 11
NZ = 12

ZTYPES = {PDG.d : ZDD,
          PDG.u : ZUU,
          PDG.s : ZSS,
          PDG.c : ZCC,
          PDG.b : ZBB,
          PDG.nu_e : ZNUENUE,
          PDG.nu_mu : ZNUMNUM,
          PDG.nu_tau : ZNUTNUT,
          PDG.e_minus : ZEE,
          PDG.mu_minus : ZMM,
          PDG.tau_minus : ZTT
          }

ZTYPNAMES = ['Zdd',
             'Zuu',
             'Zss',
             'Zcc',
             'Zbb',
             'Zne',
             'Znm',
             'Znt',
             'Zee',
             'Zmm',
             'Ztt',
             'Zxx']

class _Truthtmp(object):
    pass
def dump_one_truth (t, id0, f=sys.stdout):
    v = t.getLorentzVec()
    da = list(t.getDaughters())
    d = _Truthtmp()
    d.bc = t.id() - id0
    pdg = t.getPDG()
    d.name = pdgid_to_name (pdg)
    d.da1 = da[0].id()-id0 if len(da)>0 else 0
    d.da2 = da[1].id()-id0 if len(da)>1 else 0
    d.da3 = da[2].id()-id0 if len(da)>2 else 0
    d.m = v.M()
    d.px = v.Px()
    d.py = v.Py()
    d.pz = v.Pz()
    d.pt = v.Pt()
    d.phi = v.Phi()
    d.eta = v.Eta()
    d.e = v.Energy()

    stat = t.getGeneratorStatus()
    d.stat = stat

    print ("%(bc)3d %(name)-4s %(da1)4s %(da2)4s %(da3)4s %(pt)6.1f %(eta)5.2f %(phi)5.2f %(m)5.1f %(px)6.1f %(py)6.1f %(pz)6.1f %(e)6.1f %(stat)d" % d.__dict__, file=f)



class Dumper( Driver ):
    def startOfData (self):
        self.n = 0
    def processEvent (self, event):
        if self.n%1 == 0: print ('---', self.n)
        self.n = self.n + 1
        mcParticles = event.getMcParticles()
        id0 = mcParticles[0].id()-1
        for p in mcParticles:
            dump_one_truth (p, id0)
        # stack = [mcParticles[0], mcParticles[1], mcParticles[2], mcParticles[3]]
        # vtot = None
        # while stack:
        #     #print ('stack', [(p.id() - id0, p.getPDG()) for p in stack])
        #     p = stack.pop(0)
        #     if p.getGeneratorStatus() == 1:
        #         v = p.getLorentzVec()
        #         #print ('aa', p.id() - id0, v.Px(), v.Py(), v.Pz(), v.Energy())
        #         if vtot:
        #             vtot = vtot + v
        #         else:
        #             vtot = v
        #     else:
        #         for da in list(p.getDaughters()):
        #             if da not in stack:
        #                 stack.append (da)

        return
    def endOfData (self):
        pass


class Filter( Driver ):
    def __init__ (self, fileName):
        Driver.__init__( self )
        self.n = 0
        self.nz = 0
        self.nsel = 0
        self.ztypes = NZ*[0]
        self.zztypes = NZ*[NZ*[0]]
        return

    def startOfData( self ):
        return
 
    def processRunHeader( self, run ):
        return
    
    def processEvent( self, event ):
        self.n += 1
        mcParticles = event.getMcParticles()
        id0 = mcParticles[0].id()-1
        ip = 0
        z1 = None
        for p in mcParticles:
            if p.getPDG() == PDG.Z0:
                da = list(p.getDaughters())
                if len(da) >= 2:
                    da1 = da[0].id()-id0
                    did = abs (mcParticles[da1].getPDG())
                    ztype = ZTYPES.get(did, ZBAD)
                    self.ztypes[ztype] += 1
                    self.nz += 1
                    if z1 == None:
                        z1 = ztype
                    else:
                        self.zztypes[z1][ztype] += 1
                        return
            ip += 1
            if ip > 20: break

        return
        
    def endOfData( self ):
        print ('events:', self.n, ' nz:', self.nz)
        print ('ztypes')
        for i in range(NZ):
            print ('%s %.1f' % (ZTYPNAMES[i], self.ztypes[i]/self.nz*100))
        print ('TOT %.1f' % (sum(self.ztypes)/self.nz*100))
        return
 


def filter (infile, ofile):
    from pyLCIO.io.EventLoop import EventLoop
    eventLoop = EventLoop()
    eventLoop.addFile( infile )

    #dumper = Dumper()
    #eventLoop.add( dumper )

    f = Filter( ofile )
    eventLoop.add( f )

    eventLoop.skipEvents( 0 )
    eventLoop.loop( 500 )
    return

#import sys
#infile = sys.argv[1]
#if len(sys.argv) >= 3:
#    ofile = sys.argv[2]
#else:
#    ofile = None


filter ('/usatlas/u/chweber/usatlasdata/UOregon_montecarlo2021_ilc250_whizard_4f_ZZ/sidmc21a_ilc250_eLpR_ZZ.4.whizard_2_6_4.stdhep', None)



