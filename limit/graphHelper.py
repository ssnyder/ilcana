import ROOT
import ctypes

def getTGraphWithoutError( errorTGraph , ySetpoint = "median"):

    noErrorTGraph = ROOT.TGraph()

    xCopy =ctypes.c_double() # use these for pass by reference
    yCopy = ctypes.c_double() # use these for pass by reference

    #if   ySetpoint == "median":  getXY = lambda n : errorTGraph.GetPoint(n,xCopy,yCopy)
    #elif ySetpoint == "median":  getXY = lambda n : errorTGraph.GetPoint(n,xCopy,yCopy)

    for n in range(0,errorTGraph.GetN() ): 
        errorTGraph.GetPoint(n,xCopy,yCopy)
        if   ySetpoint == "yHigh": yCopy = ctypes.c_double( yCopy.value + errorTGraph.GetErrorYhigh(n) )
        elif ySetpoint == "yLow":  yCopy = ctypes.c_double( yCopy.value + errorTGraph.GetErrorYlow(n) )
        noErrorTGraph.SetPoint(n,xCopy,yCopy)

    return noErrorTGraph


def createNamedTGraphAsymmErrors( objectName):
    graph = ROOT.TGraphAsymmErrors()
    graph.SetName(objectName)
    return graph

def fillTGraphWithRooRealVar(graph, xFill, yFill):


    pointNr = graph.GetN()

    graph.SetPoint( pointNr, xFill, yFill.getVal() )

    yErrorHi = abs( yFill.getMax()-yFill.getVal() )
    yErrorLo = abs( -yFill.getMin() + yFill.getVal() )
    graph.SetPointError( pointNr, 0,0, yErrorLo , yErrorHi )

    return graph


