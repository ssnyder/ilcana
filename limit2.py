import string
import math
import ROOT
from ROOT import RooWorkspace, RooRealVar, RooDataHist, RooArgSet, RooHistPdf, \
    RooCategory, RooSimultaneous,RooArgList, RooDataSet, RooKeysPdf, \
    RooPolynomial
from ROOT import RooFit
from ROOT.RooStats import ProfileLikelihoodCalculator, AsymptoticCalculator, HypoTestInverter
from brzdll import brzdll
from interpolate import interpolate
from draw_obj import zone, draw_obj, printpdf
import random

if 'style_loaded' not in globals():
    ROOT.gROOT.LoadMacro ('AtlasStyle.C')
    ROOT.SetAtlasStyle()
    ROOT.TGaxis.SetMaxDigits(4)
    style_loaded = True

fb = 1
ifb = 1
ab = 1e-3 * fb

# Important parameters
ilum = 2000 * ifb

# Factor by which to scale xsect_fid so that it is on the order of unity.
sscale = 1000

# SM H production cross section, in fb.  From hzz whizard sample.
sigzh = 319 * fb

# Input file
f = ROOT.TFile('limit.root')

#################

chans = ['all', 'eeee', 'eemm', 'mmmm']
all = 0
eeee = 1
eemm = 2
mmmm = 3
nchan = 4

#eff = [0.39, 0.32, 0.38, 0.46]
#acc = [0.76, 0.16, 0.44, 0.17]
#mean = [39.8, 39.7, 39.8, 39.9]
#wid = [0.36, 0.30, 0.34, 0.31]


def makeKeysPdf (name, h_raw, var):
    if h_raw.GetEntries() <= 10:
        # RooKeysPdf blows up if there's only one entry.
        # And if there are only a few events, we get wiggly limits...
        bkg = RooPolynomial (name, name, var, RooArgList(), 1)
        return (bkg, None)
    
    rand = random.Random()
    rand.seed (hash (name))
    ds = RooDataSet (name + '_ds', name + '_ds', var)
    nbin = h_raw.GetNbinsX()
    for i in range(nbin):
        n = h_raw.GetBinContent (i)
        for j in range(int(n)):
            lo = h_raw.GetBinLowEdge(i)
            hi = h_raw.GetBinLowEdge(i+1)
            x = rand.uniform (lo, hi)
            var.setVal (x)
            ds.add (var)
    rk = RooKeysPdf (name, name, var, ds)
    return (rk, ds)


def makeBkg (w, d, bname, mname, chan, ilum, avgm, hmlm, useKeys = True):
    hb = getattr(d, f'{hmlm}SR_{bname}_{chan}_medium_avgm').Clone(f'hb_{mname}_{chan}')
    hb.Scale (ilum)

    if useKeys:
        hb_raw = getattr(d, f'{hmlm}SR_{bname}_{chan}_medium_avgm_raw').Clone(f'hb_raw_{mname}_{chan}')
        bkg, ds = makeKeysPdf (f'bkg{bname}_{chan}', hb_raw, avgm)
    else:
        bkg_data = RooDataHist (f'bkg{bname}_data_{chan}', f'bkg{bname}_data_{chan}',
                                RooArgSet (avgm), hb)
        bkg = RooHistPdf (f'bkg{bname}_{chan}', f'bkg{bname}_{chan}', RooArgSet (avgm), bkg_data)
    getattr(w, 'import')(bkg)
    return bkg, hb.Integral()
    

def makeModelChan (w, name, chan, ilum, eff, acc, sig_mean, sig_wid,
                   h_hzz, h_nonres, hmlm,
                   useKeys = True):
    w.factory (f'eff_{chan}[{eff}]')
    w.factory (f'sig_mean_{chan}[{sig_mean}]')
    w.factory (f'sig_wid_{chan}[{sig_wid}]')
    w.factory (f'Gaussian::sigshape_{chan}(avgm, sig_mean_{chan}, sig_wid_{chan})')
    if acc == None:
        w.factory (f'prod::snorm_{chan}(xsect_fid, ilum, eff_{chan}, inv_sscale)')
    else:
        w.factory (f'acc_{chan}[{acc}]')
        w.factory (f'prod::snorm_{chan}(xsect, ilum, eff_{chan}, acc_{chan}, inv_sscale)')

    avgm = w.var('avgm')

    bkg_hzz, bnorm_hzz = makeBkg (w, h_hzz, 'hzz', name, chan, ilum, avgm, hmlm)
    bkg_nonres, bnorm_nonres = makeBkg (w, h_nonres, 'nonres', name, chan, ilum, avgm, hmlm)

    w.factory (f'bnormhzz_{chan}[{bnorm_hzz}]')
    w.factory (f'bnormnonres_{chan}[{bnorm_nonres}]')
    #w.factory (f'Poisson::bnormnonres_{chan}(avgm, {bnorm_nonres})')
    w.factory(f'SUM::model_{chan}(bnormhzz_{chan}*bkghzz_{chan}, bnormnonres_{chan}*bkgnonres_{chan}, snorm_{chan}*sigshape_{chan})')

    return


def makeWS (name, chan, ilum, sscale = sscale):
    w = RooWorkspace (f'w_{name}_{chan}', f'w_{name}_{chan}')
    w.factory (f'ilum[{ilum}]')
    w.factory (f'avgm[0,64]')
    w.factory (f'inv_sscale[{1/sscale}]')
    return w

# acc=None means use fiducial xsect
def makeModel (name, chan, ilum, eff, acc, sig_mean, sig_wid,
               h_hzz, h_nonres, hmlm):
    w = makeWS (name, chan, ilum)
    poi_name = 'xsect_fid' if acc==None else 'xsect'
    w.factory (f'{poi_name}[0, 100]')
    makeModelChan (w, name, chan, ilum, eff, acc, sig_mean, sig_wid,
                   h_hzz, h_nonres, hmlm)
        

    mc = ROOT.RooStats.ModelConfig('modelConfig', w)
    mc.SetPdf (w.pdf(f'model_{chan}'))
    mc.SetParametersOfInterest (poi_name)
    mc.SetObservables ('avgm')

    return mc


def makeCombModel (name, ilum, eff, acc, sig_mean, sig_wid, h_hzz, h_nonres, hmlm):
    w = makeWS (name, 'all', ilum)
    w.factory (f'xsect[0, 100]')
    chanCat = RooCategory ('chanCat', 'chanCat')
    model_comb = RooSimultaneous ('model_comb', 'model_comb', chanCat)
    for i, c in enumerate(chans):
        if c == 'all': continue
        makeModelChan (w, name, c, ilum, eff[i], acc[i], sig_mean[i], sig_wid[i],
                       h_hzz, h_nonres, hmlm)
        chanCat.defineType (c)
        model_comb.addPdf (w.pdf(f'model_{c}'), c)
    getattr(w, 'import')(chanCat)
    getattr(w, 'import')(model_comb)

    mc = ROOT.RooStats.ModelConfig('modelConfig_comb', w)
    mc.SetPdf (w.pdf(f'model_comb'))
    mc.SetParametersOfInterest ('xsect')
    mc.SetObservables ('avgm')
    mc.chanCat = chanCat

    return mc


def profileLimit (mc, cl = 0.95):
    poi = mc.GetParametersOfInterest().first()
    bkg_data = mc.GetWorkspace().embeddedData ('bkg_data_all')

    pl = ProfileLikelihoodCalculator (bkg_data, mc)
    pl.SetConfidenceLevel (0.95)
    interval = pl.GetInterval()
    return interval.UpperLimit(poi)


def clsLimit (mc, cl = 0.95):
    #bkg_data = mc.GetWorkspace().embeddedData ('bkg_data_all')

    mcClone = mc.Clone (mc.GetName() + '_clone')
    mcClone.SetSnapshot (mcClone.GetParametersOfInterest())
    mcb = mc.Clone(mc.GetName() + '_bgOnly')
    poib = mcb.GetParametersOfInterest().first()
    poib.setVal(0)
    mcb.SetSnapshot (RooArgSet (poib))

    avgm = mcClone.GetWS().var('avgm')
    if hasattr (mc, 'chanCat'):
        al = RooArgList (mc.chanCat, avgm)
    else:
        al = RooArgList (avgm)
    null_data = ROOT.RooDataSet ('null_data', 'null_data', al)

    ac = AsymptoticCalculator (null_data, mcb, mcClone)
    ac.SetOneSided (True)
    inv = HypoTestInverter (ac)
    inv.SetConfidenceLevel (0.95)
    inv.UseCLs (True)
    inv.SetVerbose (False)
    inv.SetFixedScan (60, 0, 20)
    interval = inv.GetInterval()

    return (interval.GetExpectedUpperLimit(0),
            (interval.GetExpectedUpperLimit(-1),
             interval.GetExpectedUpperLimit(1)),
            (interval.GetExpectedUpperLimit(-2),
             interval.GetExpectedUpperLimit(2)))


class LimitData:
    def __init__ (self, d, tag=''):
        self.d = d
        self.g_acc = [getattr(d, f'g_acc_{tag}all'),
                      getattr(d, f'g_acc_{tag}eeee'),
                      getattr(d, f'g_acc_{tag}eemm'),
                      getattr(d, f'g_acc_{tag}mmmm')]
        self.g_eff = [getattr(d, f'g_eff_{tag}all'),
                      getattr(d, f'g_eff_{tag}eeee'),
                      getattr(d, f'g_eff_{tag}eemm'),
                      getattr(d, f'g_eff_{tag}mmmm')]
        self.g_mean = [getattr(d, f'g_mean_{tag}all'),
                       getattr(d, f'g_mean_{tag}eeee'),
                       getattr(d, f'g_mean_{tag}eemm'),
                       getattr(d, f'g_mean_{tag}mmmm')]
        self.g_wid = [getattr(d, f'g_wid_{tag}all'),
                      getattr(d, f'g_wid_{tag}eeee'),
                      getattr(d, f'g_wid_{tag}eemm'),
                      getattr(d, f'g_wid_{tag}mmmm')]

        acc_all = getattr(d, f'g_acc_{tag}all')
        n = acc_all.GetN()
        self.mzd = [acc_all.GetPointX(i) for i in range(n)]

        self.acc = []
        self.eff = []
        self.mean = []
        self.wid = []
        for i in range(n):
            self.acc.append ([self.g_acc[c].GetPointY(i) for c in range(nchan)])
            self.eff.append ([self.g_eff[c].GetPointY(i) for c in range(nchan)])
            self.mean.append ([self.g_mean[c].GetPointY(i) for c in range(nchan)])
            self.wid.append ([self.g_wid[c].GetPointY(i) for c in range(nchan)])
        return

class Limits:
    def __init__ (self, ilum, limitData, mzd):
        self.mzd = mzd
        name = 'zd' + str(self.mzd)
        if name.endswith('.0'): name = name[:-2]
        print ('name', name)
        eff  = [ginterp(g, mzd) for g in limitData.g_eff]
        acc  = [ginterp(g, mzd) for g in limitData.g_acc]
        mean = [ginterp(g, mzd) for g in limitData.g_mean]
        wid  = [ginterp(g, mzd) for g in limitData.g_wid]
        hmlm = 'HM' if mzd >= 15 else 'LM'
        self.mc_all = makeModel (name, 'all', ilum,
                                 eff[all],
                                 None,
                                 mean[all],
                                 wid[all],
                                 limitData.d.hzz,
                                 limitData.d.nonres,
                                 hmlm)
        self.mc_eeee = makeModel (name, 'eeee', ilum,
                                  eff[eeee],
                                  None,
                                  mean[eeee],
                                  wid[eeee],
                                  limitData.d.hzz,
                                  limitData.d.nonres,
                                  hmlm)
        self.mc_eemm = makeModel (name, 'eemm', ilum,
                                  eff[eemm],
                                  None,
                                  mean[eemm],
                                  wid[eemm],
                                  limitData.d.hzz,
                                  limitData.d.nonres,
                                  hmlm)
        self.mc_mmmm = makeModel (name, 'mmmm', ilum,
                                  eff[mmmm],
                                  None,
                                  mean[mmmm],
                                  wid[mmmm],
                                  limitData.d.hzz,
                                  limitData.d.nonres,
                                  hmlm)
        self.mc_comb = makeCombModel (name, ilum,
                                      eff, acc, mean, wid,
                                      limitData.d.hzz,
                                      limitData.d.nonres,
                                      hmlm)

        # These in ab.
        self.fid_all = self.scale (1 / sscale / ab, clsLimit (self.mc_all))
        self.fid_eeee =  self.scale (1 / sscale / ab, clsLimit (self.mc_eeee))
        self.fid_eemm =  self.scale (1 / sscale / ab, clsLimit (self.mc_eemm))
        self.fid_mmmm =  self.scale (1 / sscale / ab, clsLimit (self.mc_mmmm))
        self.xstot =  self.scale (1 / sscale / ab, clsLimit (self.mc_comb))

        brscale = ab / sigzh / brzdll(self.mzd)**2
        self.br = self.scale (brscale, self.xstot)
        return

    def scale (self, s, cl):
        return (cl[0]*s,
                (cl[1][0]*s, cl[1][1]*s),
                (cl[2][0]*s, cl[2][1]*s))


    def form (self, lim):
        return '%.2f +%.2f-%.2f  +%.2f-%.2f' % \
            (lim[0],
             lim[1][1]-lim[0], lim[0]-lim[1][0],
             lim[2][1]-lim[0], lim[0]-lim[2][0])


def ginterp (g, x):
    table = list (zip (list (g.GetX()), list (g.GetY())))
    if x < 15:
        table = [p for p in table if p[0] <= 15]
    else:
        table = [p for p in table if p[0] >= 15]
    return interpolate (table, x, 3)


def make_limits (data, lo, hi, step):
    out = []
    mzd = lo
    while mzd <= hi:
        out.append (Limits (ilum, data, mzd))
        mzd += step
    return out




objs = []

def scale_graph (g, s):
    gnew = g.Clone (g.GetName() + '_scaled')
    for i in range(gnew.GetN()):
        gnew.SetPointY (i, gnew.GetPointY(i)*s)
        gnew.SetPointError (i, 0, gnew.GetErrorY(i)*s)
    return gnew

def label (x, y, text, font=42, size=0.03):
    l = ROOT.TLatex()
    l.SetNDC()
    l.SetTextFont(font)
    l.SetTextColor(1)
    l.SetTextSize(size)
    l.DrawLatex (x, y, text)
    objs.append (l)
    return

def ilclabel (x=0.70, y=0.85):
    label (x, y, 'ILC', font=72, size=0.05)
    label (x, y-0.05, '#sqrt{s}=250 GeV, %d fb^{-1}' % ilum)
    
    return

def channel_legend (gobjs,
                    x1 = 0.2, y1 = 0.75, x2 = 0.3, y2 = 0.9):
    leg = ROOT.TLegend (x1, y1, x2, y2)
    leg.AddEntry (gobjs[eeee], '4e', 'LF')
    leg.AddEntry (gobjs[eemm], '2e2#mu', 'LF')
    leg.AddEntry (gobjs[mmmm], '4#mu', 'LF')
    leg.SetBorderSize (0)
    leg.Draw()
    objs.append(leg)
    return


def divline (ymax):
    l = ROOT.TLine (15, 0, 15, ymax)
    l.SetLineStyle (2)
    objs.append(l)
    l.Draw()
    return

def acceff_plot1 (g_in, c, hm_p):
    g_in.SetLineWidth(2)
    g = g_in.Clone (g_in.GetName() + '_clone')
    objs.append (g)
    i = 0
    while i < g.GetN():
        x = g.GetPointX(i)
        if (hm_p and x < 15) or (not hm_p and x > 15):
            g.RemovePoint (i)
        else:
            i += 1
    if isinstance (g, ROOT.TGraphErrors):
        g.SetLineColor(1)
        g_in.SetLineColor(1)
        g.SetFillColor(c-9)
        g_in.SetFillColor(c-9)
        draw_obj (g, 'SAME,3')
    else:
        g.SetLineColor(c)
        g_in.SetLineColor(c)
    draw_obj (g, 'SAME,LX')
    return
def acceff_plot (hm_gobjs, lm_gobjs, title):
    zone (1,1)
    frame = ROOT.TH1F('frame', '', 10, 0, 60)
    frame.SetMinimum(0)
    frame.SetMaximum(1)
    frame.GetXaxis().SetTitle ('m_{Zd} [GeV]')
    frame.GetYaxis().SetTitle (title + ' [unitless]')
    draw_obj (frame)
    objs.append (frame)
    acceff_plot1 (hm_gobjs[eeee], ROOT.kRed, True)
    acceff_plot1 (hm_gobjs[eemm], ROOT.kBlue, True)
    acceff_plot1 (hm_gobjs[mmmm], ROOT.kOrange, True)
    acceff_plot1 (lm_gobjs[eeee], ROOT.kRed, False)
    acceff_plot1 (lm_gobjs[eemm], ROOT.kBlue, False)
    acceff_plot1 (lm_gobjs[mmmm], ROOT.kOrange, False)
    divline (1)
    channel_legend (hm_gobjs)
    ilclabel()
    return
def eff_plot (hm, lm):
    acceff_plot (hm.g_eff, lm.g_eff, 'Efficiency')
    printpdf ('plots/4l-efficiency.pdf')
    return
def acc_plot (hm, lm):
    hm_gobs = [hm.g_acc[all],
               scale_graph (hm.g_acc[eeee], 4),
               scale_graph (hm.g_acc[eemm], 2),
               scale_graph (hm.g_acc[mmmm], 4)]
    lm_gobs = [lm.g_acc[all],
               scale_graph (lm.g_acc[eeee], 4),
               scale_graph (lm.g_acc[eemm], 2),
               scale_graph (lm.g_acc[mmmm], 4)]
    acceff_plot (hm_gobs, lm_gobs, 'Acceptance')
    printpdf ('plots/4l-acceptance.pdf')
    return


def mean_plot (hm, lm):
    zone (1,1)
    frame = ROOT.TH1F('frame', '', 10, 0, 60)
    frame.SetMinimum(0)
    frame.SetMaximum(60)
    frame.GetXaxis().SetTitle ('m_{Zd} [GeV]')
    frame.GetYaxis().SetTitle ('Gaussian mean [GeV]')
    draw_obj (frame)
    objs.append (frame)
    acceff_plot1 (hm.g_mean[eeee], ROOT.kRed, True)
    acceff_plot1 (hm.g_mean[eemm], ROOT.kBlue, True)
    acceff_plot1 (hm.g_mean[mmmm], ROOT.kOrange, True)
    acceff_plot1 (lm.g_mean[eeee], ROOT.kRed, False)
    acceff_plot1 (lm.g_mean[eemm], ROOT.kBlue, False)
    acceff_plot1 (lm.g_mean[mmmm], ROOT.kOrange, False)
    divline (60)
    channel_legend (hm.g_mean)
    ilclabel()
    printpdf ('plots/4l-mean.pdf')
    return


def wid_plot (hm, lm):
    zone (1,1)
    frame = ROOT.TH1F('frame', '', 10, 0, 60)
    frame.SetMinimum(0)
    frame.SetMaximum(1)
    frame.GetXaxis().SetTitle ('m_{Zd} [GeV]')
    frame.GetYaxis().SetTitle ('Gaussian width [GeV]')
    draw_obj (frame)
    objs.append (frame)
    acceff_plot1 (hm.g_wid[eeee], ROOT.kRed, True)
    acceff_plot1 (hm.g_wid[eemm], ROOT.kBlue, True)
    acceff_plot1 (hm.g_wid[mmmm], ROOT.kOrange, True)
    acceff_plot1 (lm.g_wid[eeee], ROOT.kRed, False)
    acceff_plot1 (lm.g_wid[eemm], ROOT.kBlue, False)
    acceff_plot1 (lm.g_wid[mmmm], ROOT.kOrange, False)
    channel_legend (hm.g_wid)
    divline (1)
    ilclabel()
    printpdf ('plots/4l-wid.pdf')
    return


def limit_plot (lims, ylab, max=12):
    frame = ROOT.TH1F('frame', '', 10, 0, 60)
    frame.SetMinimum(0)
    frame.SetMaximum(max)
    frame.GetXaxis().SetTitle ('m_{Zd} [GeV]')
    frame.GetYaxis().SetTitle ('95% CL upper limit on ' + ylab)
    draw_obj (frame)
    objs.append (frame)
    g = ROOT.TGraph (len (lims))
    g1 = ROOT.TGraphAsymmErrors (len (lims))
    g2 = ROOT.TGraphAsymmErrors (len (lims))
    objs.append (g)
    objs.append (g1)
    objs.append (g2)
    for i, (mzd, r) in enumerate (lims):
        g.SetPoint (i, mzd, r[0])
        g1.SetPoint (i, mzd, r[0])
        g1.SetPointEYlow (i, r[0]-r[1][0])
        g1.SetPointEYhigh (i, r[1][1]-r[0])
        g2.SetPoint (i, mzd, r[0])
        lo = r[2][0]
        if math.isinf(lo): lo = 0
        g2.SetPointEYlow (i, r[0]-lo)
        g2.SetPointEYhigh (i, r[2][1]-r[0])
    g2.SetFillColor(5)
    g1.SetFillColor(3)
    draw_obj (g2, 'SAME,3')
    draw_obj (g1, 'SAME,3')
    draw_obj (g, 'SAME')
    ilclabel(y=0.9)

    leg = ROOT.TLegend (0.37, 0.77, 0.67, 0.92)
    leg.SetTextFont(42)
    leg.AddEntry (g, 'Expected', 'L')
    leg.AddEntry (g1, 'Expected #pm 1#sigma', 'F')
    leg.AddEntry (g2, 'Expected #pm 2#sigma', 'F')
    leg.SetBorderSize (0)
    leg.Draw()
    divline (max)
    objs.append(leg)
    f2 = frame.Clone()
    f2.Draw('SAME,AXIS')
    objs.append(f2)
    return


def fid_plot (hm_limits, lm_limits, chan):
    if chan == eeee:
        cname2 = '4e'
        cname = 'eeee'
    elif chan == eemm:
        cname2 = '2e2#mu'
        cname = 'eemm'
    elif chan == mmmm:
        cname2 = '4#mu'
        cname = 'mmmm'

    lims = [(l.mzd, getattr (l, f'fid_{cname}')) for l in lm_limits+hm_limits]

    limit_plot (lims, '#sigma_{fid} [ab]')
    label (0.7, 0.8, f'{cname2} final state')
    printpdf (f'plots/4l-fid_{cname}.pdf')
    return


def xstot_plot (hm_limits, lm_limits):
    lims = [(l.mzd, l.xstot) for l in lm_limits+hm_limits]

    limit_plot (lims, 'total cross section [ab]', max=18)
    printpdf ('plots/4l-xstot.pdf')
    return


def br_plot (hm_limits, lm_limits):
    lims = [(l.mzd, l.br) for l in lm_limits+hm_limits]
    limit_plot (lims, '#frac{#sigma}{#sigma^{SM}_{H+X}}B(H#rightarrow Z_{d}Z_{d})', max=1e-3)
    printpdf ('plots/4l-br.pdf')
    return


def allplots (HMLimitData, LMLimitData, limits):
    eff_plot (HMLimitData, LMLimitData)
    acc_plot (HMLimitData, LMLimitData)
    mean_plot (HMLimitData, LMLimitData)
    wid_plot (HMLimitData, LMLimitData)
    fid_plot (hm_limits, lm_limits, eeee)
    fid_plot (hm_limits, lm_limits, eemm)
    fid_plot (hm_limits, lm_limits, mmmm)
    xstot_plot (limits)
    br_plot (limits)
    return
    
    
HMLimitData = LimitData(f.HMSR)
LMLimitData = LimitData(f.LMSR, 'lm_')
#hm_limits = make_limits (HMLimitData, 15, 60, 1)
#lm_limits = make_limits (LMLimitData, 1, 14, 1) + [Limits(ilum, LMLimitData, 14.99)]
#limits = make_limits (limitData, 15, 60, 1)
#limit40 = Limits (ilum, limitData, 40)
#limit55 = Limits (ilum, limitData, 55)
#h_scaled=limitData.d.hzz.HMSR_hzz_eeee_medium_avgm
#h_raw=limitData.d.hzz.HMSR_hzz_eeee_medium_avgm_raw
#avgm = list(limits[10].mc_eeee.GetObservables())[0]
#plot = ROOT.RooPlot('plot', 'plot', avgm, 0, 80, 80)
    


#limits = []
#for i in range (len (limitData.mzd)):
#    limits.append (Limits (ilum, limitData, i))
#for limit in limits:
#    print ('---', limit.mzd)
#    print ('fid_all (ab): ', limit.form (limit.fid_all))
#    print ('fid_eeee (ab):', limit.form (limit.fid_eeee))
#    print ('fid_eemm (ab):', limit.form (limit.fid_eemm))
#    print ('fid_mmmm (ab):', limit.form (limit.fid_mmmm))
#    print ('xstot (ab):', limit.form (limit.xstot))
#    print ('br (*1e-3): ', limit.form(limit.scale (1000, limit.br)))


#mc_all = makeModel ('zd40', 'all', ilum, eff[all], None, mean[all], wid[all], f.HMSR.hzz,)
#mc_eeee = makeModel ('zd40', 'all', ilum, eff[eeee], None, mean[eeee], wid[eeee], f.HMSR.hzz)
#mc_eemm = makeModel ('zd40', 'all', ilum, eff[eemm], None, mean[eemm], wid[eemm], f.HMSR.hzz)
#mc_mmmm = makeModel ('zd40', 'all', ilum, eff[mmmm], None, mean[mmmm], wid[mmmm], f.HMSR.hzz)
##mc_comb = makeCombModel ('zd40', ilum, eff, acc, mean, wid, f.HMSR.hzz)

#mc.Print ('t')
#mc.GetWorkspace().Print('t')

#pl = profileLimit (mc_all)
#cl = clsLimit (mc_eeee)
#print ('pl', pl)
#print ('cl', cl)

def execfile (f):
    exec (open(f).read(), globals())


