# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

#
# File: truth.py
# Created: sss, March 2005.
# Purpose: Dump out MC truth information.
#

"""Dump out MC truth information.

This module contains the function dump_truth which will dump out the
truth information for the current event in a tabular format.
"""

import PDG
import sys
import os
from math import hypot, atan2, log, sqrt

def asinh (x):
    return - log (sqrt (x*x+1) - x)


class _Truthtmp(object):
    pass
def dump_one_truth (t, i, f=sys.stdout):
    d = _Truthtmp()
    d.bc = i
    d.name = PDG.pdgid_to_name(t.mcpdg[i])
    d.da1 = t.mcda0[i]
    d.da2 = t.mcda1[i]
    d.da3 = t.mcda2[i]
    d.m = t.mcmas[i]
    d.px = t.mcmox[i]
    d.py = t.mcmoy[i]
    d.pz = t.mcmoz[i]
    d.pt = hypot (d.px, d.py)
    d.phi = atan2 (d.py, d.px)

    if d.pt == 0 or abs(d.pz/d.pt)>1e10:
        if d.pz > 0: d.eta = 9.99
        else: d.eta = -9.99
    else:
        d.eta = asinh (d.pz / d.pt)
    d.e = sqrt (d.px**2 + d.py**2 + d.pz**2 + d.m**2)
    print ("%(bc)3d %(name)-4s %(da1)4s %(da2)4s %(da3)4s %(pt)6.1f %(eta)5.2f %(phi)5.2f %(m)5.1f %(px)6.1f %(py)6.1f %(pz)6.1f %(e)6.1f" % d.__dict__, file=f)


def dump_truth (t, maxn=None, f=sys.stdout):
    if maxn == None:
        maxn = t.nmcp
    else:
        maxn = min (t.nmcp, maxn)
    for i in range(maxn):
        dump_one_truth (t, i, f)
    return

def mtruth (t, *ndxs):
    px_tot = 0
    py_tot = 0
    pz_tot = 0
    e_tot = 0
    for i in ndxs:
        m = t.mcmas[i]
        px = t.mcmox[i]
        py = t.mcmoy[i]
        pz = t.mcmoz[i]
        e = sqrt(m**2 + px**2 + py**2 + pz**2)
        px_tot += px
        py_tot += py
        pz_tot += pz
        e_tot += e
    m2 = e_tot**2 - px_tot**2 - py_tot**2 - pz_tot**2
    if m2 >= 0: return sqrt(m2)
    return -sqrt(-m2)


def dumpme(ndx):
    t = uo_all_sm2
    t.GetEntry(ndx)
    dump_truth(t)
    print('|%.1f| %.1f| %.1f|' % (mtruth(t, 2, 3), mtruth(t, 4, 5),mtruth(t, 2, 3, 4, 5)))
    print('|%.1f| %.1f| %.1f|' % (mtruth(t, 2, 4), mtruth(t, 3, 5),mtruth(t, 2, 3, 4, 5)))
    print('|%.1f| %.1f| %.1f|' % (mtruth(t, 2, 5), mtruth(t, 3, 4),mtruth(t, 2, 3, 4, 5)))
    return


metacache = {}
def procnum():
    t = uo_all_sm2
    evt = t.evevt
    fname = t.GetFile().GetName()
    fbase = os.path.basename (fname)
    pos = fbase.find ('.sim')
    fbase = fbase[:pos]
    metaname = '/usatlas/u/chweber/usatlasdata/UOregonBackgrounds_1X_reco/metadata/' + fbase + '.delphes_card_DSiDi.tcl.root.txt'
    print (evt, metaname)
    if metaname not in metacache:
        procs = []
        for l in open(metaname).readlines():
            ll = l.split()
            assert int(ll[0]) == len(procs)
            procs.append (int(ll[1]))
        metacache[metaname] = procs
    return metacache[metaname][evt]
    



def ana_truth (t):
    z_nu = 0
    z_l = 0
    z_tau = 0
    z_had = 0

    h_b = 0
    h_w = 0
    h_z = 0
    h_z4l = 0
    h_tau = 0
    h_gam = 0
    h_had = 0
    h_g = 0
    h_l = 0

    pdg_emu = (PDG.e_minus, PDG.mu_minus)

    nent = t.GetEntries()
    for ievt in range(nent):
        t.GetEntry(ievt)
        if ievt%1000 == 0: print (ievt)
        zdaid = abs(t.mcpdg[2])
        if zdaid in (PDG.nu_e, PDG.nu_mu, PDG.nu_tau):
            z_nu += 1
        elif zdaid in pdg_emu:
            z_l += 1
        elif zdaid == PDG.tau_minus:
            z_tau += 1
        elif zdaid in (PDG.d, PDG.u, PDG.s, PDG.c, PDG.t, PDG.b):
            z_had += 1
        else:
            print ('unknown Z decay', ievt, zdaid)

        hdaid = abs(t.mcpdg[5])
        if hdaid == PDG.b:
            h_b += 1
        elif hdaid == PDG.W_plus:
            h_w += 1
        elif hdaid == PDG.Z0:
            h_z += 1
            if (abs(t.mcpdg[7]) in pdg_emu and
                abs(t.mcpdg[8]) in pdg_emu and                
                abs(t.mcpdg[9]) in pdg_emu and                
                abs(t.mcpdg[10]) in pdg_emu):
                h_z4l += 1
        elif hdaid == PDG.tau_minus:
            h_tau += 1
        elif hdaid == PDG.gamma:
            h_gam += 1
        elif hdaid == PDG.g:
            h_g += 1
        elif hdaid in (PDG.d, PDG.u, PDG.s, PDG.c, PDG.t):
            h_had += 1
        elif hdaid in pdg_emu:
            h_l += 1
        else:
            print ('unknown H decay', ievt, hdaid)
            

    def printone (lab, count):
        print (lab, count, count/ nent)
        return

    printone ('Z->nu:', z_nu)
    printone ('Z->l:',  z_l)
    printone ('Z->tau:',  z_tau)
    printone ('Z->had:',  z_had)
    printone ('H->b:',  h_b)
    printone ('H->W:',  h_w)
    printone ('H->Z:',  h_z)
    printone ('H->Z4l:',  h_z4l)
    printone ('H->tau:',  h_tau)
    printone ('H->gam:',  h_gam)
    printone ('H->had:',  h_had)
    printone ('H->g:',  h_g)
    printone ('H->l:',  h_l)
    return



def ana_truth2 (t):
    ecut = 5
    ecut2 = ecut*ecut
    ptcut = 1
    ptcut2 = ptcut*ptcut
    nent = t.GetEntries()
    for ievt in range(nent):
        t.GetEntry(ievt)
        if ievt%1000 == 0: print (ievt)
        np = min (t.nmcp, 50)
        ne = 0
        for ip in range(np):
            if abs(t.mcpdg[ip]) != 13 or t.mcda0[ip] != -1: continue
            m = t.mcmas[ip]
            px = t.mcmox[ip]
            py = t.mcmoy[ip]
            pz = t.mcmoz[ip]
            pt2 = px*px + py*py
            e2 = pt2 + pz*pz + m*m
            if e2 > ecut2 and pt2 > ptcut2:
                ne = ne+1
                if ne >= 4: break
        if ne >= 4:
            print ('found', ievt)


