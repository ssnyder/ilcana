import ROOT
import glob
import os
from array import array

def make_chain (name, files):
    ch = ROOT.TChain (name)
    if not isinstance (files, list): files = [files]
    flist = []
    for f in files:
        flist += glob.glob (f)
    flist.sort()
    for ff in flist:
        ch.Add (ff + '?#MyLCTuple')
    return ch


def read_procs (fname, ifile):
    fbase = os.path.basename (fname)
    pos = fbase.find ('.stdhep')
    fbase = fbase[:pos]
    metaname = f'/usatlas/u/chweber/usatlasdata/UOregonBackgrounds_1X_reco/metadata/{fbase}{ifile:02}.stdhep.delphes_card_DSiDi.tcl.root.txt'
    procs = []
    for l in open(metaname).readlines():
        ll = l.split()
        assert int(ll[0]) == len(procs)
        procs.append (int(ll[1]))
    return procs


def makeproc (t, ofile):
    last_fname = None
    last_iFile = None
    procs = []
    tproc = ROOT.TTree (t.GetName() + '_proc', t.GetName() + '_proc')
    proc_buf = array ('i', [0])
    tproc.proc_buf = proc_buf
    tproc.Branch ('proc', proc_buf, 'proc/i')
    for i in range (t.GetEntries()):
        t.GetEntry(i)
        fname = t.GetFile().GetName()
        if fname != last_fname or last_iFile != t.iFile:
            procs = read_procs (fname, t.iFile)
            last_fname = fname
            last_iFile = t.iFile
        proc_buf[0] = procs[t.evevt]
        tproc.Fill()
    f = ROOT.TFile.Open (ofile, 'RECREATE')
    tproc.Write()
    f.Close()
    return tproc
            
 
ch = make_chain ('uo_all_sm2',  '../data/tuple/uo_all_sm2/*_reco-filt.root')
makeproc (ch, 'uo_all_sm2_proc.root')
