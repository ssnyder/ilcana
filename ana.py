import ROOT
import cppyy
import glob
from array import array
from draw_obj import zone, draw_obj, printpdf
from math import sqrt
from twiki_table import twiki_table
from SampleBase import SampleBase

print ('reading ana.py')

if 'code' not in globals():
    code = open('ana.cc').read()
    ROOT.gInterpreter.Declare(code)

#ROOT.EnableImplicitMT()

def make_chain (name, files):
    ch = ROOT.TChain (name)
    if not isinstance (files, list): files = [files]
    flist = []
    for f in files:
        flist += glob.glob (f)
    flist.sort()
    for ff in flist:
        ch.Add (ff + '?#MyLCTuple')
    return ch

def getproc (i, t, data):
    t.GetEntry(i)
    fname = t.GetFile().GetName()
    index = t.index
    evt = t.evevt
    part = int(index / 500)
    assert index%500 == evt, (index, evt, i)
    return 0

def add_index (t):
    n = t.GetEntries()
    tindex = ROOT.TTree (t.GetName() + '_index', t.GetName() + '_index')
    tindex.SetAutoFlush(0)
    tindex.findex_buf = array ('Q', [0])
    br_findex = tindex.Branch ('findex', tindex.findex_buf, 'findex/l')
    br_findex.SetBasketSize (8*n + 1024)
    doindex = not tindex.GetBranch('index')
    if doindex:
        tindex.index_buf = array ('Q', [0])
        br_index = tindex.Branch ('index', tindex.index_buf, 'index/l')
        br_index.SetBasketSize (8*n + 1024)

    data = []
    for i in range(n):
        tindex.findex_buf[0] = i
        if doindex:
            tindex.index_buf[0] = i
        tindex.Fill()
    t.tindex = tindex
    t.AddFriend (tindex, 'tindex')
    return


def add_proc (t, proc):
    f = ROOT.TFile.Open (proc)
    tproc = getattr (f, t.GetName() + '_proc')
    t.AddFriend (tproc, 'tproc')
    t.proc_f = f
    t.proc_t = tproc
    return



class KinHists:
    def __init__ (self, r, lab, max_e = None, max_m = None,
                  n_m2 = None, max_m2 = None):
        if max_e == None: max_e = 100
        if max_m == None: max_m = 100

        self.r = r
        self.vn = self.r.Define (f'{lab}_vn', f'(int){lab}.size()')
        self.n = self.vn.Histo1D (f'{lab}_vn')

        self.ve = self.r.Define (f'{lab}_ve', f'get_e({lab})')
        self.e = self.ve.Histo1D (ROOT.RDF.TH1DModel (f'{lab}_e',
                                                      f'{lab}_e',
                                                      50, 0, max_e),
                                  f'{lab}_ve')

        self.vth = self.ve.Define (f'{lab}_vth', f'get_th({lab})')
        self.th = self.vth.Histo1D (ROOT.RDF.TH1DModel (f'{lab}_th',
                                                        f'{lab}_th',
                                                        50, 0, 3.5),
                                    f'{lab}_vth')

        self.vphi = self.vth.Define (f'{lab}_vphi', f'get_phi({lab})')
        self.phi = self.vphi.Histo1D (ROOT.RDF.TH1DModel (f'{lab}_phi',
                                                          f'{lab}_phi',
                                                          50, -3.5, 3.5),
                                      f'{lab}_vphi')

        self.vm = self.vphi.Define (f'{lab}_vm', f'get_m({lab})')
        self.m = self.vm.Histo1D (ROOT.RDF.TH1DModel (f'{lab}_m',
                                                      f'{lab}_m',
                                                      50, 0, max_m),
                                  f'{lab}_vm')

        if n_m2 != None and max_m2 != None:
            self.m2 = self.vm.Histo1D (ROOT.RDF.TH1DModel (f'{lab}_m',
                                                           f'{lab}_m',
                                                           n_m2, 0, max_m2),
                                       f'{lab}_vm')
        return


class QuadHists:
    def __init__ (self, r, base, lab):
        self.base = base
        h1 = self.h1
        h1 (r, lab, 'e',      50,  0,  200)
        h1 (r, lab, 'th',     50,  0,    3.5)
        h1 (r, lab, 'phi',    50, -3.5,  3.5)
        h1 (r, lab, 'm4l',    50,  0,  200)
        h1 (r, lab, 'm12',    50,  0,  200)
        h1 (r, lab, 'm34',    50,  0,  200)
        h1 (r, lab, 'm14',    50,  0,  200)
        h1 (r, lab, 'm23',    50,  0,  200)

        h1 (r, lab, 'avgm',   80,  0,  160)
        h1 (r, lab, 'avgm_fine',   200,  0,  100, 'avgm')
        h1 (r, lab, 'dm',     50,  0,  100)
        h1 (r, lab, 'mratio', 50,  0,    1)
        h1 (r, lab, 'min_el_e', 50, 0, 100)
        h1 (r, lab, 'min_mu_e', 50, 0, 100)
        h1 (r, lab, 'min_el_pt', 50, 0, 100)
        h1 (r, lab, 'min_mu_pt', 50, 0, 100)
        h1 (r, lab, 'max_el_aeta', 50, 0, 4)
        h1 (r, lab, 'max_mu_aeta', 50, 0, 4)
        h1 (r, lab, 'min_sf_dr', 50, 0, 5)
        h1 (r, lab, 'min_of_dr', 50, 0, 5)
        #self.h2 ('m1m2', 50, 0, 150*gev, 50, 0, 150*gev, scatter=True)
        return


    def h1 (self, r, lab, var, nx, xlo, xhi, meth = None):
        if meth is None: meth = var
        vname = var + '_v'
        v = r.Define (f'{lab}_{vname}', f'{self.base}[0].{meth}()')
        h = v.Histo1D (ROOT.RDF.TH1DModel (f'{lab}_{var}',
                                           f'{lab}_{var}',
                                           nx, xlo, xhi),
                       f'{lab}_{vname}')
        setattr (self, vname, v)
        setattr (self, var, h)
        return
        


class Ana:
    def __init__ (self, tree, mconly = False, do_mchwind = False):
        self.mzd = tree.mzd
        self.xsec = tree.xsec
        self.ra_n = None
        self.ra = None

        self.tree = tree
        self.rdf = ROOT.RDataFrame (tree)
        self.definedNames = []

        if mconly:
            return self.setupMCAna (tree, do_hwind = do_mchwind)

        return self.setupFullAna (tree, do_mchwind = do_mchwind)


    def setupMCAna (self, tree, do_hwind = False):
        self.setupMCDefs (tree)
        self.setupMCCuts (tree, mconly = True, do_hwind = do_hwind)
        self.setupMCLMCuts (tree, mconly = True, do_hwind = do_hwind)
        return


    def setupMCDefs (self, tree):
        self.vmc = self.define ('vmc', 'make_mc(mcpdg, mcgst, mcmox, mcmoy, mcmoz, mcene, evevt)')
        self.mc = KinHists (self.vmc, 'vmc')

        self.defKin('mc',    'mc_muoraw', 'filterCont(vmc, atypStaEThetaMCFilter(13, 1, 7, 0.35))')
        self.defKin('mc_muoraw', 'mc_eleraw', 'filterCont(vmc, atypStaEThetaMCFilter(11, 1, 7, 0.35))')

        self.defKin ('mc_eleraw', 'mc_gam', 'filterCont(vmc, atypStaEThetaMCFilter(22, 1, 0.5, 0.1))')
        #self.vmcdr_ele = self.vmc_gam.Define ('vmcdr_ele', 'get_min_dr(vmc_ele, vmc_gam)')
        #self.mcdr_ele = self.vmcdr_ele.Histo1D (ROOT.RDF.TH1DModel ('mcdr_ele',
        #                                                            'mcdr_ele',
        #                                                            50, 0, 5),
        #                                        'vmcdr_ele')

        self.defKin('mc_gam',    'mc_muo', 'dress(vmc_muoraw, vmc_eleraw, vmc_gam)')
        self.defKin('mc_muo',    'mc_ele', 'dress(vmc_eleraw, vmc_muoraw, vmc_gam)')

        self.defKin ('mc_ele', 'mc_muopairs', 'make_lpairs(vmc_muo)',
                     n_m2 = 60, max_m2 = 15)
        self.defKin ('mc_muopairs', 'mc_elepairs', 'make_lpairs(vmc_ele)',
                     n_m2 = 60, max_m2 = 15)

        # Form quadruplets with sfos pairs.
        self.defKin ('mc_elepairs', 'mc_quad', 'make_quads(vmc_elepairs, vmc_muopairs)', max_m = 200)

        # Lepton kinematics: E1 > 20, E2 > 15, E3 > 10
        self.defKin ('mc_quad', 'mc_quad_kin', 'filterCont(vmc_quad, quadKinCut)', max_m = 200)

        # SF DR > 0.1; OF DR > 0.2
        self.defKin ('mc_quad_kin', 'mc_quad_dr', 'filterCont(vmc_quad_kin, quadDRCut)', max_m = 200)

        return


    def setupMCCuts (self, tree, mconly, do_hwind = False):
        self.cut_mc_anyquad = self.vmc_quad.Filter ('!vmc_quad.empty()', 'mc_anyquad')
        self.cut_mc_lkin = self.vmc_quad_kin.Filter ('!vmc_quad_kin.empty()', 'mc_lkin')

        # Require at least one quad.
        self.cut_mc_all = self.vmc_quad_dr.Filter ('!vmc_quad_dr.empty()', 'mc_quad')

        self.mc_quad_h = QuadHists (self.cut_mc_all, 'vmc_quad_dr', 'mc_quad')

        # Separate into channels.
        self.cut_mc_eeee = self.cut_mc_all.Filter ('vmc_quad_dr[0].typ() == 1111' ,'mc_eeee')
        self.cut_mc_eemm = self.cut_mc_all.Filter ('vmc_quad_dr[0].typ() == 1122 || vmc_quad_dr[0].typ() == 2211 ', 'mc_eemm')
        self.cut_mc_mmmm = self.cut_mc_all.Filter ('vmc_quad_dr[0].typ() == 2222', 'mc_mmmm')


        for chan in ['eeee', 'eemm', 'mmmm', 'all']:
            self.add_mc_chanCut (chan, 'jpsi', '',
                                 'oniaVeto (vmc_quad_dr[0], 3.096-0.25, 3.686+0.30)',
                                 mconly = mconly)
            self.add_mc_chanCut (chan, 'upsilon', 'jpsi',
                                 'oniaVeto (vmc_quad_dr[0], 9.461-0.70, 10.355+0.75)',
                                 mconly = mconly)
            self.add_mc_chanCut (chan, 'lowmveto', 'upsilon',
                                 'oniaVeto (vmc_quad_dr[0], 0, 5)',
                                 mconly = mconly)

            lastcut = 'lowmveto'
            if do_hwind:
                self.add_mc_chanCut (chan, 'hwind', 'lowmveto',
                                     '115 < vmc_quad_dr[0].m4l() && vmc_quad_dr[0].m4l() < 130',
                                     mconly = mconly)
                lastcut= 'hwind'
            self.add_mc_chanCut (chan, 'zveto', lastcut, 'ZVeto (vmc_quad_dr[0])',
                                 mconly = mconly)
            self.add_mc_chanCut (chan, 'loose', 'zveto',
                                 'vmc_quad_dr[0].m12() > 10 && vmc_quad_dr[0].m34() > 10',
                                 mconly = mconly)
            self.add_mc_chanCut (chan, 'medium', 'loose',
                                 'vmc_quad_dr[0].mratio() > 0.85',
                                 mconly = mconly)
        return


    def setupMCLMCuts (self, tree, mconly, do_hwind = False):
        # Require at least one quad.
        self.cut_mc_lm_all = self.vmc_quad_kin.Filter ('!vmc_quad_kin.empty()', 'mc_lm_quad')

        self.mc_lm_quad_h = QuadHists (self.cut_mc_lm_all, 'vmc_quad_kin', 'mc_lm_quad')

        # Separate into channels.
        self.cut_mc_lm_eeee = self.cut_mc_lm_all.Filter ('vmc_quad_kin[0].typ() == 1111' ,'mc_lm_eeee')
        self.cut_mc_lm_eemm = self.cut_mc_lm_all.Filter ('vmc_quad_kin[0].typ() == 1122 || vmc_quad_kin[0].typ() == 2211 ', 'mc_lm_eemm')
        self.cut_mc_lm_mmmm = self.cut_mc_lm_all.Filter ('vmc_quad_kin[0].typ() == 2222', 'mc_lm_mmmm')


        for chan in ['lm_eeee', 'lm_eemm', 'lm_mmmm', 'lm_all']:
            self.add_mc_chanCut (chan, 'jpsi', '',
                                 'oniaVeto_alt (vmc_quad_kin[0], 3.096-0.25, 3.686+0.30)',
                                 quadtype = 'kin', mconly = mconly)
            self.add_mc_chanCut (chan, 'upsilon', 'jpsi',
                                 'oniaVeto_alt (vmc_quad_kin[0], 9.461-0.70, 10.355+0.75)',
                                 quadtype ='kin', mconly = mconly)
            #self.add_mc_chanCut (chan, 'jpsi2', 'upsilon',
            #                     'oniaVeto_lm (vmc_quad_kin[0], 2, 4.4)',
            #                     quadtype = 'kin', mconly = mconly)
            #self.add_mc_chanCut (chan, 'upsilon2', 'jpsi2',
            #                     'oniaVeto_lm (vmc_quad_kin[0], 8, 12)',
            #                     quadtype = 'kin', mconly = mconly)
            lastcut = 'upsilon'
            if do_hwind:
                self.add_mc_chanCut (chan, 'hwind', 'upsilon',
                                     '120 < vmc_quad_kin[0].m4l() && vmc_quad_kin[0].m4l() < 130',
                                     quadtype = 'kin', mconly = mconly)
                lastcut= 'hwind'
            self.add_mc_chanCut (chan, 'loose', lastcut,
                                 'vmc_quad_kin[0].m12()>0.8 && vmc_quad_kin[0].m12()<20 && vmc_quad_kin[0].m34()>0.8 && vmc_quad_kin[0].m34()<20',
                                 quadtype = 'kin', mconly = mconly)
            self.add_mc_chanCut (chan, 'medium', 'loose',
                                 'vmc_quad_kin[0].mratio() > 0.85',
                                 quadtype = 'kin', mconly = mconly)
        return


    def setupLMCuts (self, tree, do_mchwind = False):
        self.cut_lm_all = self.vquad_kin.Filter ('!vquad_kin.empty()', 'lm_quad')
        self.lm_quad_h = QuadHists (self.cut_lm_all, 'vquad_kin', 'lm_quad')

        # Separate into channels.
        self.cut_lm_eeee = self.cut_lm_all.Filter ('vquad_kin[0].typ() == 1111' ,'eeee')
        self.cut_lm_eemm = self.cut_lm_all.Filter ('vquad_kin[0].typ() == 1122 || vquad_kin[0].typ() == 2211 ', 'eemm')
        self.cut_lm_mmmm = self.cut_lm_all.Filter ('vquad_kin[0].typ() == 2222', 'mmmm')

        for chan in ['lm_eeee', 'lm_eemm', 'lm_mmmm', 'lm_all']:
            self.add_chanCut (chan, 'jpsi', '',
                              'oniaVeto_alt (vquad_kin[0], 3.096-0.25, 3.686+0.30)',
                              base = 'vquad_kin')
            self.add_chanCut (chan, 'upsilon', 'jpsi',
                              'oniaVeto_alt (vquad_kin[0], 9.461-0.70, 10.355+0.75)',
                              base = 'vquad_kin')
            #self.add_chanCut (chan, 'jpsi2', 'upsilon',
            #                  'oniaVeto_lm (vquad_kin[0], 2, 4.4)',
            #                  base = 'vquad_kin')
            #self.add_chanCut (chan, 'upsilon2', 'jpsi2',
            #                  'oniaVeto_lm (vquad_kin[0], 8, 12)',
            #                  base = 'vquad_kin')
            self.add_chanCut (chan, 'hwind', 'upsilon',
                              '120 < vquad_kin[0].m4l() && vquad_kin[0].m4l() < 130',
                              base = 'vquad_kin')
            self.add_chanCut (chan, 'loose', 'hwind',
                              'vquad_kin[0].m12()>0.8 && vquad_kin[0].m12()<20 && vquad_kin[0].m34()>0.8 && vquad_kin[0].m34()<20',
                              base = 'vquad_kin')
            self.add_chanCut (chan, 'medium', 'loose',
                              'vquad_kin[0].mratio() > 0.85',
                              base = 'vquad_kin')

                              

        ###################################################################


        # Also RP+MC
        self.cut_mcrp_lm_all = self.cut_lm_all_medium.Filter ('!vmc_quad_kin.empty()' ,'mcrp_lm_all')
        self.cut_mcrp_lm_eeee = self.cut_lm_eeee_medium.Filter ('!vmc_quad_kin.empty() && vmc_quad_kin[0].typ() == 1111' ,'mcrp_lm_eeee')
        self.cut_mcrp_lm_eemm = self.cut_lm_eemm_medium.Filter ('!vmc_quad_kin.empty() && (vmc_quad_kin[0].typ() == 1122 || vmc_quad_kin[0].typ() == 2211)', 'mcrp_lm_eemm')
        self.cut_mcrp_lm_mmmm = self.cut_lm_mmmm_medium.Filter ('!vmc_quad_kin.empty() && vmc_quad_kin[0].typ() == 2222', 'mcrp_lm_mmmm')

        self.setupMCLMCuts (tree, mconly = False, do_hwind = do_mchwind)

        return


    def setupFullAna (self, tree, do_mchwind = False):
        self.evwgt_h = self.rdf.Histo1D ('evwgt')
        self.evsig_h = self.rdf.Histo1D ('evsig')

        self.setupMCDefs (tree)

        #####################################################################
        # RecoParts

        self.defKin ('mc_quad_dr', 'alljets', 'make_jet(jori, jmox, jmoy, jmoz, jene)')
        self.defKin('alljets',  'j2', 'filterCont(valljets, oriJetFilter(12))',
                    max_e = 200, max_m = 150)
        self.defKin ('j2', 'j2pairs', 'make_jet_pairs(vj2)', max_m = 150,
                     max_e = 200)

        #self.cut_zwindow = self.vj2pairs.Filter ('massWindow(vj2pairs, 80, 110)',
        self.cut_zwindow = self.vj2pairs.Filter ('true',
                                                 'zwindow')
        self.cut_zwindow_h = KinHists (self.cut_zwindow,
                                       'vj2pairs',
                                       max_m = 120)

        
        self.defKin ('cut_zwindow', 'rps', 'make_rp(rctyp, rcori, rcmox, rcmoy, rcmoz, rcene)')

        # Lists of good leptons with E > 7.
        self.defKin ('rps',  'muo', 'filterCont(vrps, atypOriRPFilter(13, 2))')
        self.defKin ('muo',  'muo1', 'filterCont(vmuo, EThetaFilter(7, 0.35))')
        self.defKin ('muo1', 'ele', 'filterCont(vrps, atypOriRPFilter(11, 1))')
        self.defKin ('ele',  'ele1', 'filterCont(vele, EThetaFilter(7, 0.35))')

        self.defKin ('ele1', 'muopairs', 'make_lpairs(vmuo1)',
                     n_m2 = 60, max_m2 = 15)
        self.defKin ('muopairs', 'elepairs', 'make_lpairs(vele1)',
                     n_m2 = 60, max_m2 = 15)

        self.defKin ('rps',  'lmmuo', 'filterCont(vrps, atypOriRPFilter(13, 6))')
        self.defKin ('lmmuo',  'lmmuo1', 'filterCont(vlmmuo, EThetaFilter(7, 0.35))')
        self.defKin ('lmmuo1', 'lmele', 'filterCont(vrps, atypOriRPFilter(11, 7))')
        self.defKin ('lmele',  'lmele1', 'filterCont(vlmele, EThetaFilter(7, 0.35))')

        self.defKin ('lmele1', 'lmmuopairs', 'make_lpairs(vlmmuo1)',
                     n_m2 = 60, max_m2 = 15)
        self.defKin ('lmmuopairs', 'lmelepairs', 'make_lpairs(vlmele1)',
                     n_m2 = 60, max_m2 = 15)

        # Form quadruplets with sfos pairs.
        self.defKin ('elepairs', 'quad', 'make_quads(velepairs, vmuopairs)', max_m = 200)

        # Lepton kinematics: E1 > 20, E2 > 15, E3 > 10
        self.defKin ('quad', 'quad_kin', 'filterCont(vquad, quadKinCut)', max_m = 200)

        # SF DR > 0.1; OF DR > 0.2
        self.defKin ('quad_kin', 'quad_dr', 'filterCont(vquad_kin, quadDRCut)', max_m = 200)

        # Require at least one quad.
        self.cut_all = self.vquad_dr.Filter ('!vquad_dr.empty()', 'quad')

        self.quad_h = QuadHists (self.cut_all, 'vquad_dr', 'quad')

        # Separate into channels.
        self.cut_eeee = self.cut_all.Filter ('vquad_dr[0].typ() == 1111' ,'eeee')
        self.cut_eemm = self.cut_all.Filter ('vquad_dr[0].typ() == 1122 || vquad_dr[0].typ() == 2211 ', 'eemm')
        self.cut_mmmm = self.cut_all.Filter ('vquad_dr[0].typ() == 2222', 'mmmm')

        if hasattr (tree, 'proc'):
            self.vprint_eeee = self.cut_eeee.Define ('vprint_eeee', 'print_ev(evrun, evevt,index,findex,iFile,proc,vquad_dr[0].typ())')
            self.print_eeee_h = self.vprint_eeee.Histo1D ('vprint_eeee')
            self.vprint_eemm = self.cut_eemm.Define ('vprint_eemm', 'print_ev(evrun, evevt,index,findex,iFile,proc,vquad_dr[0].typ())')
            self.print_eemm_h = self.vprint_eemm.Histo1D ('vprint_eemm')
            self.vprint_mmmm = self.cut_mmmm.Define ('vprint_mmmm', 'print_ev(evrun, evevt,index,findex,iFile,proc,vquad_dr[0].typ())')
            self.print_mmmm_h = self.vprint_mmmm.Histo1D ('vprint_mmmm')


        for chan in ['eeee', 'eemm', 'mmmm', 'all']:
            self.add_chanCut (chan, 'jpsi', '',
                              'oniaVeto (vquad_dr[0], 3.096-0.25, 3.686+0.30)')
            self.add_chanCut (chan, 'upsilon', 'jpsi',
                              'oniaVeto (vquad_dr[0], 9.461-0.70, 10.355+0.75)')
            self.add_chanCut (chan, 'lowmveto', 'upsilon',
                              'oniaVeto (vquad_dr[0], 0, 5)')
            self.add_chanCut (chan, 'hwind', 'lowmveto',
                              '115 < vquad_dr[0].m4l() && vquad_dr[0].m4l() < 130')
            self.add_chanCut (chan, 'zveto', 'hwind', 'ZVeto (vquad_dr[0])')
            self.add_chanCut (chan, 'loose', 'zveto',
                              'vquad_dr[0].m12() > 10 && vquad_dr[0].m34() > 10')
            self.add_chanCut (chan, 'medium', 'loose',
                              'vquad_dr[0].mratio() > 0.85')

                              

        ###################################################################


        # Also RP+MC
        self.cut_mcrp_all = self.cut_all_medium.Filter ('!vmc_quad_dr.empty()' ,'mcrp_all')
        self.cut_mcrp_eeee = self.cut_eeee_medium.Filter ('!vmc_quad_dr.empty() && vmc_quad_dr[0].typ() == 1111' ,'mcrp_eeee')
        self.cut_mcrp_eemm = self.cut_eemm_medium.Filter ('!vmc_quad_dr.empty() && (vmc_quad_dr[0].typ() == 1122 || vmc_quad_dr[0].typ() == 2211)', 'mcrp_eemm')
        self.cut_mcrp_mmmm = self.cut_mmmm_medium.Filter ('!vmc_quad_dr.empty() && vmc_quad_dr[0].typ() == 2222', 'mcrp_mmmm')

        self.setupMCCuts (tree, mconly = False, do_hwind = do_mchwind)

        #self.xprint = self.cut_mcrp_mmmm_upsilon.Define ('xprint', '!ZVeto(vmc_quad_dr[0]) && xprint_ev(evevt, index)')
        #self.xprint_h = self.xprint.Histo1D ('xprint')

        self.setupLMCuts (tree, do_mchwind = do_mchwind)
        return


    def add_chanCut (self, chan, cut, prior, code, base = 'vquad_dr'):
        priorname = f'cut_{chan}'
        if prior: priorname = priorname + '_' + prior
        cutname = f'cut_{chan}_{cut}'
        cutlab = f'{chan}_{cut}'
        c = getattr (self, priorname).Filter (code, cutlab)
        setattr (self, cutname, c)
        h = QuadHists (c, base, cutlab)
        setattr (self, cutlab + '_h', h)
        return

    def add_mc_chanCut (self, chan, cut, prior, code, mconly = False,
                        quadtype = 'dr'):
        self.add_chanCut ('mc_' + chan, cut, prior, code, 'vmc_quad_' + quadtype)
        if not mconly:
            self.add_chanCut ('mcrp_' + chan, cut, prior, code, 'vmc_quad_' + quadtype)
        return


    def define (self, name, code):
        self.definedNames.append (name)
        return self.rdf.Define (name, code)


    def defKin (self, frm, to, code, **kw):
        if hasattr (self, 'v'+frm): frm = 'v' + frm
        d = getattr (self, frm).Define ('v'+to, code)
        self.definedNames.append ('v'+to)
        h = KinHists (d, 'v'+to, **kw)
        setattr (self, 'v'+to, d)
        setattr (self, to, h)
        return d


    def __getitem__ (self, n):
        if n == self.ra_n: return self.ra
        self.ra = rtest (self.tree, n)
        self.ra_n = n
        return self.ra
        

fb = 1
ab = fb/1000
ifb = 1

class Sample (SampleBase):
    def __init__ (self, name, fpat, xsec = 1*fb, mconly = False, do_mchwind = False):
        SampleBase.__init__ (self)
        self._name = name
        self._fpat = fpat
        self._xsec = xsec
        self._mconly = mconly
        self._do_mchwind = do_mchwind
        return

    def _create (self):
        self._pyname = 'a_' + self._name
        ch = make_chain (self._name, self._fpat)
        self._gdict[self._name] = ch

        mzd = 0
        if self._name.startswith ('zd'):
            nm = self._name[2:]
            if nm.endswith ('lm'):
                nm = nm[:-2]
            if nm.endswith ('lm2'):
                nm = nm[:-3]
            if nm.endswith ('aa'):
                nm = nm[:-2]
            mzd = float(nm)
        ch.mzd = mzd
        ch.xsec = self._xsec

        a = Ana(ch, mconly = self._mconly, do_mchwind = self._do_mchwind)
        self._gdict['a_' + self._name] = a
        return a
        
        
def defsamp (name, fpat, proc = None, xsec = 1*fb, mconly = False, do_mchwind = False):
    if name in globals(): return
    globals()['a_' + name] = Sample (name, fpat, xsec, mconly, do_mchwind)
    # ch = make_chain (name, fpat)
    # globals()[name] = ch
    # #add_index (ch)
    # #if proc: add_proc (ch, proc)

    # mzd = 0
    # if name.startswith ('zd'):
    #     mzd = float(name[2:])
    # ch.mzd = mzd
    # ch.xsec = xsec

    # globals()['a_' + name] = Ana(ch, mconly = mconly, do_mchwind = do_mchwind)
    return

defsamp ('zd01',   '../data/tuple/zd01_20000/*_rec.root')
defsamp ('zd02',   '../data/tuple/zd02_20000/*_rec.root')
defsamp ('zd04',   '../data/tuple/zd04_20000/*_rec.root')
defsamp ('zd06',   '../data/tuple/zd06_20000/*_rec.root')
defsamp ('zd08',   '../data/tuple/zd08_20000/*_rec.root')
defsamp ('zd10',   '../data/tuple/zd10_20000/*_rec.root')
defsamp ('zd12',   '../data/tuple/zd12_20000/*_rec.root')
defsamp ('zd15',   '../data/tuple/zd15_20000/*_rec.root')
defsamp ('zd20',   '../data/tuple/zd20_20000/*_rec.root')
defsamp ('zd25',   '../data/tuple/zd25_20000/*_rec.root')
defsamp ('zd30',   '../data/tuple/zd30_20000/*_rec.root')
defsamp ('zd35',   '../data/tuple/zd35_20000/*_rec.root')
defsamp ('zd40',   '../data/tuple/zd40_20000/*_rec.root')
defsamp ('zd45',   '../data/tuple/zd45_20000/*_rec.root')
defsamp ('zd50',   '../data/tuple/zd50_20000/*_rec.root')
defsamp ('zd55',   '../data/tuple/zd55_20000/*_rec.root')
defsamp ('zd60',   '../data/tuple/zd60_20000/*_rec.root')
#defsamp ('hzz',    '../data/tuple/hzz_500_rec.root')
#defsamp ('hzzwiz', '../data/tuple/hzzwiz_20000/*.root', xsec = 205*fb * (5.0741912E-07 /  4.1430000E-03))
defsamp ('hzz', '../data/tuple/hzz/*.root', xsec = 319*fb * (5.0778780E-07 / 4.1430000E-03))
defsamp ('h2f',    '../data/tuple/2f1h/ilc250*.root')
defsamp ('nonres', '../data/tuple/nonres*/*.root', xsec = 54.5*fb)
#defsamp ('nonres2', '../data/tuple/nonres2/*.root', xsec = 54.5*fb)
#defsamp ('nonres3', '../data/tuple/nonres3/*.root', xsec = 54.5*fb)
#defsamp ('nonres4', '../data/tuple/nonres4/*.root', xsec = 54.5*fb)
#defsamp ('nonres5', '../data/tuple/nonres5/*.root', xsec = 54.5*fb)

#defsamp ('uo_all_sm',    '../data/tuple/uo_all_sm/*_reco.root')
#defsamp ('uo_all_sm2',  '../data/tuple/uo_all_sm2/*_reco-filt.root', 'uo_all_sm2_proc.root')

#defsamp ('uo_zz_lr1_filt', '../data/tuple/uo_zz_lr1_filt/*_rec.root')

#defsamp ('zz4e', '../whiztest/test6/test6_eeee.root', mconly=True, do_mchwind = True)
#defsamp ('zz4m', '../whiztest/test6/test6_mmmm.root', mconly=True, do_mchwind = True)
#defsamp ('zzem', '../whiztest/test6/test6_eemm.root', mconly=True, do_mchwind = True)
#defsamp ('zzall6', '../whiztest/test6/test6_all.root', mconly=True, do_mchwind = True)
#defsamp ('zzall7', '../whiztest/test7_all/test7_all.root', mconly=True, do_mchwind = True)
#defsamp ('zzall7r', '../data/tuple/test7_all/*_rec.root')

#defsamp ('zd02aa', '../data/tuple/zd02_20000/*_01_rec.root')
#defsamp ('zd02lm', '../data/tuple_lm/zd02_20000/*_01_rec.root')
#defsamp ('zd02lm2', '../data/tuple_lm2/zd02_20000/*_01_rec.root')
defsamp ('bbar', '../data/tuple/bbar/*.root')
defsamp ('bbar_lm', '../data/tuple_lm2/bbar/*.root')

defsamp ('psipsi8uu',   '../data/tuple/psipsi8uu/*_rec.root', do_mchwind = True, xsec = 45*ab)
defsamp ('psipsi8dd',   '../data/tuple/psipsi8dd/*_rec.root', do_mchwind = True, xsec = 11*ab)
defsamp ('psipsi8ss',   '../data/tuple/psipsi8ss/*_rec.root', do_mchwind = True, xsec = 11*ab)
defsamp ('psipsi8bb',   '../data/tuple/psipsi8bb/*_rec.root', do_mchwind = True, xsec = 8.3*ab)
defsamp ('psipsi8cc',   '../data/tuple/psipsi8cc/*_rec.root', do_mchwind = True, xsec = 41*ab)
defsamp ('psipsi8g',   '../data/tuple/psipsi8g/*_rec.root', do_mchwind = True, xsec = 0.060*ab)
defsamp ('psipsi',   '../data/tuple/psipsi/*_rec.root', do_mchwind = True, xsec =8.3*ab)
defsamp ('upup',   '../data/tuple/upup/*_rec.root', do_mchwind = True, xsec = 0.18*ab)
defsamp ('zffpsipsi',   '../data/tuple/zffjpsijpsi/*_rec.root', do_mchwind = True, xsec = 0.22*ab)
defsamp ('zffupup',   '../data/tuple/zffupup/*_rec.root', do_mchwind = True, xsec = 0.025*ab)


#zone(1,1)


class ValDesc:
    def __init__ (self, val):
        self.val = val
    def __get__ (self, obj, objtype=None):
        return self.val.GetValue().get()
def createSliceVar (ThisAna, sl, n):
    typ = sl.GetColumnType(n)
    fillercls = ROOT.ColumnHolder(typ)
    ff = ROOT.CallFill[fillercls]()
    val = ff.fill (sl, n)
    nn = str(n) + '_val'
    desc = ValDesc (val)
    setattr (ThisAna, nn, desc)
    return
def rtest (tree, n=0):
    sl = ROOT.MakeSliceDataFrame (tree, n, 1)
    sl.mzd = tree.mzd
    sl.xsec = tree.xsec
    class ThisAna (Ana):
        pass
    a = ThisAna (sl)
    a.rdf = sl

    for n in sl.GetColumnNames():
        createSliceVar (ThisAna, sl, n)
    for n in a.definedNames:
        createSliceVar (ThisAna, getattr(a, n), n)
    return a


objs = []

def markline (h, x, d=0):
    y = h.GetMaximum()*0.75
    l = ROOT.TLine (x, 0, x, y)
    l.SetLineColor(2)
    l.Draw()
    objs.append(l)
    if d:
        l2 = ROOT.TArrow (x, y, x + d*h.GetXaxis().GetXmax()*0.05, y)
        l2.SetLineColor(2)
        l2.SetArrowSize (0.01)
        l2.Draw()
        objs.append(l2)
    return l
def plot1 (a, tag, hname, title, marks = []):
    h = getattr (getattr (a, tag), hname)
    h.SetTitle (title)
    draw_obj (h)
    for i in range (len (marks)//2):
        markline (h, marks[2*i], marks[2*i+1])
    return
def quadplots1 (a, chan='all', cut = None):
    zone(2,2)
    tag = f'{chan}_{cut}_h' if cut else 'quad_h'
    plot1 (a, tag, 'm12', f'm_12: best quadruplet, mZd = {a.mzd}', [64, -1])
    plot1 (a, tag, 'm34', f'm_34: best quadruplet, mZd = {a.mzd}', [64, -1])
    if chan != 'eemm' and chan != 'all':
        plot1 (a, tag, 'm23', f'm_23: best quadruplet, mZd = {a.mzd}', [75, -1])
        plot1 (a, tag, 'm14', f'm_14: best quadruplet, mZd = {a.mzd}', [75, -1])
    printpdf (f'plots/quadplots1_{a.mzd}.pdf')
    return


def quadplots2 (a, chan='all', cut = None):
    zone(2,2)
    tag = f'{chan}_{cut}_h' if cut else 'quad_h'
    plot1 (a, tag, 'avgm', f'(m_12+m_34)/2: best quadruplet, mZd = {a.mzd}')
    plot1 (a, tag, 'avgm_fine', f'(m_12+m_34)/2: best quadruplet, mZd = {a.mzd}')
    plot1 (a, tag, 'dm', f'|m_12-m_34|: best quadruplet, mZd = {a.mzd}')
    plot1 (a, tag, 'mratio', f'm_34/m_12: best quadruplet, mZd = {a.mzd}', [0.85, 1])
    plot1 (a, tag, 'm4l', f'm_4l: best quadruplet, mZd = {a.mzd}', [115, 1, 130, -1])
    printpdf (f'plots/quadplots2_{a.mzd}.pdf')
    return


def jetplot(a):
    zone(1,1)
    a.j2pairs.m.SetTitle (f'm_jj, mZd = {a.mzd}')
    draw_obj (a.j2pairs.m)
    markline (a.j2pairs.m, 80, 1)
    markline (a.j2pairs.m, 110, -1)
    printpdf (f'plots/mjj_{a.mzd}.pdf')
    return


def cutflow (a, chan='all', cut = None):
    tag = f'cut_{chan}_{cut}' if cut else 'cut_quad'
    getattr(a, tag).Report().Print()
    return


#??? what if i've opened multiple trees?
#ra = rtest (ROOT.MyLCTuple, 10)

#a.cut_quad.Report().Print()

def get_scaled (h, name, a, ilum):
    scale = ilum * a.xsec / a.tree.GetEntries()
    h = h.Clone (name)
    h.Scale (scale)
    if h.Integral() == 0:
        nbins = h.GetNbinsX()
        dum = scale / nbins
        for i in range(nbins):
            h.SetBinContent (i+1, dum)
    return h


def make_limit_hists (fout, a, tag, ilum, pref='', outpref='HMSR_'):
    olddir = ROOT.gROOT.CurrentDirectory()
    try:
        scale = ilum * a.xsec / a.tree.GetEntries()
        d = fout.mkdir (tag)
        d.cd()
        outpref= outpref + tag + '_'
        h_all_raw = getattr(a,f'{pref}all_medium_h').avgm_fine.GetValue().Clone (outpref + 'all_medium_avgm_raw')
        h_all = getattr(a,f'{pref}all_medium_h').avgm.GetValue().Clone (outpref + 'all_medium_avgm')
        h_all.Scale (scale)
        h_eeee_raw = getattr(a,f'{pref}eeee_medium_h').avgm_fine.GetValue().Clone (outpref + 'eeee_medium_avgm_raw')
        h_eeee = getattr(a,f'{pref}eeee_medium_h').avgm.GetValue().Clone (outpref + 'eeee_medium_avgm')
        h_eeee.Scale (scale)
        h_eemm_raw = getattr(a,f'{pref}eemm_medium_h').avgm_fine.GetValue().Clone (outpref + 'eemm_medium_avgm_raw')
        h_eemm = getattr(a,f'{pref}eemm_medium_h').avgm.GetValue().Clone (outpref + 'eemm_medium_avgm')
        h_eemm.Scale (scale)
        h_mmmm_raw = getattr(a,f'{pref}mmmm_medium_h').avgm_fine.GetValue().Clone (outpref + 'mmmm_medium_avgm_raw')
        h_mmmm = getattr(a,f'{pref}mmmm_medium_h').avgm.GetValue().Clone (outpref + 'mmmm_medium_avgm')
        h_mmmm.Scale (scale)
        h_all.Write()
        h_eeee.Write()
        h_eemm.Write()
        h_mmmm.Write()
        h_all_raw.Write()
        h_eeee_raw.Write()
        h_eemm_raw.Write()
        h_mmmm_raw.Write()
    finally:
        olddir.cd()
    return

def fit_signal (s, c):
    m = s.mzd
    r = getattr(s, f'{c}_medium_h').avgm_fine.Fit ('gaus', 'SQ0', '', m-5, m+5)
    print ('sig', m, c, r, r.Get())
    if not r.Get(): return (None,None)
    p = r.Get().GetParams()
    return (p[1], p[2])

def make_sig_graphs (d, siglist, pref):
    nsig = len(siglist)
    chans = [f'{pref}all', f'{pref}eeee', f'{pref}eemm', f'{pref}mmmm']
    for c in chans:
        data = []
        for i, s in enumerate (siglist):
            ntot = s.tree.GetEntries()
            n_mc_medium = getattr(s, f'cut_mc_{c}_medium').Count().GetValue()
            n_mcrp_medium = getattr(s, f'cut_mcrp_{c}_medium').Count().GetValue()
            print (c, s.mzd, n_mc_medium)
            if n_mc_medium == 0 or ntot == 0: continue
            mzd = s.mzd
            acc = n_mc_medium / ntot
            sig_acc = sqrt(acc*(1-acc)/ntot)
            eff = n_mcrp_medium / n_mc_medium
            sig_eff = sqrt(eff*(1-eff)/n_mc_medium)
            (mean, wid) = fit_signal (s, c)
            if mean == None: continue
            data.append ((mzd, (acc, sig_acc), (eff, sig_eff), mean, wid))

        n = len(data)
        g_acc = ROOT.TGraphErrors (n)
        g_acc.SetName(f'g_acc_{c}')
        g_eff = ROOT.TGraphErrors (n)
        g_eff.SetName(f'g_eff_{c}')
        g_mean = ROOT.TGraph (n)
        g_mean.SetName (f'g_mean_{c}')
        g_wid = ROOT.TGraph (n)
        g_wid.SetName (f'g_wid_{c}')
        for i in range(len(data)):
            mzd, acc, eff, mean, wid = data[i]
            g_acc.SetPoint (i, mzd, acc[0])
            g_acc.SetPointError (i, 0, acc[1])
            g_eff.SetPoint (i, mzd, eff[0])
            g_eff.SetPointError (i, 0, eff[1])
            g_mean.SetPoint (i, mzd, mean)
            g_wid.SetPoint (i, mzd, wid)
        g_acc.Write()
        g_eff.Write()
        g_mean.Write()
        g_wid.Write()
    return


def make_limit_file1 (d, signames, ilum, pref='', outpref='HMSR_'):
    sigs = []
    for sn in signames:
        print ('aaa', sn)
        import sys
        sys.stdout.flush()
        s = globals()['a_' + sn]
        if s.mzd > 10 or pref == 'lm_':
            sigs.append (s)
        make_limit_hists (d, s, sn, ilum, pref, outpref)
    print ('aaa hzz')
    make_limit_hists (d, a_hzz, 'hzz', ilum, pref, outpref)
    print ('aaa nonres')
    make_limit_hists (d, a_nonres, 'nonres', ilum, pref, outpref)
    #make_limit_hists (d, a_hzz, 'expectedData', ilum, pref, outpref)
    make_sig_graphs (d, sigs, pref)
    return

    
def make_limit_file (ilum = 1*ifb):
    fname = 'limit.root'
    olddir = ROOT.gROOT.CurrentDirectory()
    fout = ROOT.TFile.Open (fname, 'RECREATE')
    signames = get_signals()
    sigs = []
    try:
        d = fout.mkdir('HMSR')
        d.cd()
        for sn in signames:
            s = globals()['a_' + sn]
        make_limit_file1 (d, signames, ilum)

        olddir.cd()
        d = fout.mkdir('LMSR')
        d.cd()
        make_limit_file1 (d, signames, ilum, 'lm_', 'LMSR_')
        fout.Close()
    finally:
        olddir.cd()
    print ('wrote', fname)
    return

def make_lm_limit_file (ilum = 1*ifb):
    fname = 'limit_lm.root'
    olddir = ROOT.gROOT.CurrentDirectory()
    fout = ROOT.TFile.Open (fname, 'RECREATE')
    signames = get_signals()
    sigs = []
    try:
        d = fout.mkdir('LMSR')
        d.cd()
        for sn in signames:
            s = globals()['a_' + sn]
        make_limit_file1 (d, signames, ilum, 'lm_', 'LMSR_')
        fout.Close()
    finally:
        olddir.cd()
    print ('wrote', fname)
    return

def get_signals():
    out = []
    for k in globals().keys():
        if k.startswith ('a_zd'):
            out.append (k[2:])
    out.sort()
    return out


def mctable (a, chan, sig):
    ntot = a.tree.GetEntries()
    tab = twiki_table (rowfunc_flag = False,
                       title = 'sig=%.2f'%sig)
    tab.add_column ('n', lambda n: getattr(a, 'cut_'+n).Count().GetValue(),
                    just = 'r')
    tab.add_column ('eff%', lambda n: getattr(a, 'cut_'+n).Count().GetValue()/ntot*100,
                    formatter = lambda x: '%.2f'%x,
                    just = 'r')
    tab.add_column ('sig', lambda n: getattr(a, 'cut_'+n).Count().GetValue()/ntot*sig,
                    formatter = lambda x: '%.3f'%x,
                    just = 'r')
    tab.add_row ('quad', 'mc_anyquad')
    tab.add_row ('lkin', 'mc_lkin')
    tab.add_row ('dr', 'mc_all')
    tab.add_row (chan, f'mc_{chan}')
    tab.add_row ('jpsi', f'mc_{chan}_jpsi')
    tab.add_row ('upsilon', f'mc_{chan}_upsilon')
    tab.add_row ('lowmveto', f'mc_{chan}_lowmveto')
    tab.add_row ('hwind', f'mc_{chan}_hwind')
    tab.add_row ('zveto', f'mc_{chan}_zveto')
    tab.add_row ('loose', f'mc_{chan}_loose')
    tab.add_row ('medium', f'mc_{chan}_medium')
    tab.dump()
    return

def bkgtable (ilum = 2000*ifb, tex = True):
    tab = twiki_table (rowfunc_flag = False,
                       title = 'Backgrounds',
                       tex = tex)
    def form (samps, chan):
        if not isinstance(samps, list): samps = [samps]
        eff_tot = 0
        sig_eff2_tot = 0
        for a in samps:
            ntot = a.tree.GetEntries()
            n = getattr (a, f'cut_{chan}_medium').Count().GetValue()
            eff = n/ntot
            sig_eff = sqrt(eff*(1-eff)/ntot)
            scale = ilum * a.xsec
            eff_tot += eff*scale
            sig_eff2_tot += (sig_eff*scale) ** 2
        if eff_tot < 0.01:
            return '$< 0.01$'
        return '$%.2f \pm %.2f$'  % (eff_tot, sqrt(sig_eff2_tot))
    tab.add_column ('4e',   lambda a: form (a, 'eeee'), just = 'r')
    tab.add_column ('2e2m', lambda a: form (a, 'eemm'), just = 'r')
    tab.add_column ('4m',   lambda a: form (a, 'mmmm'), just = 'r')
    tab.add_column ('all',  lambda a: form (a, 'all'),  just = 'r')
    tab.add_column ('lm 4e',   lambda a: form (a, 'lm_eeee'), just = 'r')
    tab.add_column ('lm 2e2m', lambda a: form (a, 'lm_eemm'), just = 'r')
    tab.add_column ('lm 4m',   lambda a: form (a, 'lm_mmmm'), just = 'r')
    tab.add_column ('lm all',  lambda a: form (a, 'lm_all'),  just = 'r')
    tab.add_row (r'$H\rightarrow ZZ^*\rightarrow 4\ell$', a_hzz)
    tab.add_row ('Non-resonant', a_nonres)
    tab.add_row ('Total', [a_hzz, a_nonres])
    tab.dump()
    return

def execfile (f):
    exec (open(f).read(), globals())

print ('read ana.py')
