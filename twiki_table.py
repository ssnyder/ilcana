import sys


def just (s, j, w):
    if len(s) >= w: return s
    pad = ' ' * (w - len(s))
    if j == 'r':
        return pad + s
    return s + pad
def default_totfunc (data):
    tot = 0
    for d in data:
        tot += float(d)
    if tot == int(tot):
        tot = int(tot)
    return tot
class twiki_cell:
    def __init__ (self, data, formatted, for_width):
        self.data = data
        self.formatted = formatted
        self.for_width = for_width
        return
    def width (self):
        if not self.for_width: return 0
        fm = self.formatted.split ('\n')
        return max ([len(f) for f in fm])
class twiki_column:
    def __init__ (self, desc, samp, formatter, totfunc, rowfunc_flag,
                  for_width=True, just = None):
        self.samp = samp
        self.cells = [twiki_cell (None, desc, for_width)]
        #self.data = [None]
        #self.formatted = [desc]
        #self.for_width = [for_width]
        self.formatter = formatter
        self.totfunc = totfunc if totfunc else default_totfunc
        self.rowfunc_flag = rowfunc_flag
        self.max_cell_width = self.cells[0].width()
        self.just = just
        return
    def append_func (self, func, for_width = True):
        if self.rowfunc_flag:
            data = func (self.samp)
        else:
            data = self.samp (func)
        self.append_data (data, for_width)
        return
    def append_str (self, s, for_width = True):
        self.cells.append (twiki_cell (None, s, for_width))
        self.max_cell_width = max (self.max_cell_width, self.cells[-1].width())
        #self.formatted.append (s)
        #self.data.append (None)
        #self.for_width.append (for_width)
        return
    def append_data (self, data, for_width = True):
        self.cells.append (twiki_cell (data, self.formatter (data), for_width))
        self.max_cell_width = max (self.max_cell_width, self.cells[-1].width())
        #self.data.append (data)
        #self.for_width.append (for_width)
        #self.formatted.append (self.formatter (data))
        return
    def width (self):
        return self.max_cell_width+1
    def format (self, i, justtype='l'):
        thisjust = self.just
        if thisjust == None: thisjust = justtype
        out = just (self.cells[i].formatted, thisjust, self.width())
        return out
    def format_as_list (self, i, justtype='l'):
        thisjust = self.just
        if thisjust == None: thisjust = justtype
        fm = self.cells[i].formatted.split('\n')
        return [just (l, thisjust, self.width()) for l in fm]
    def total (self, first):
        self.append_data (self.totfunc ([c.data for c in self.cells[first:]]))
        return

# An object is supplied for each column, COLOBJ.
# An object is supplied for each row, ROWOBJ.
# if ROWFUNC_FLAG is true, then each cell is computed as ROWOBJ(COLOBJ);
# otherwise, COLOBJ(ROWOBJ).
class twiki_table:
    def __init__ (self, title='', formatter=str, totfunc = default_totfunc,
                  rowfunc_flag = True,
                  show_row_labels = True,
                  tex = False):
        self.labels = [title]
        self.just = ['l']
        self.columns = []
        self.formatter = formatter
        self.total_first = 1
        self.totfunc = totfunc
        self.rowfunc_flag = rowfunc_flag
        self.show_row_labels = show_row_labels
        self.tex = tex
        return
    def add_column (self, desc, samp, formatter = None, totfunc = None,
                    for_width = True, just = None):
        if formatter == None: formatter = self.formatter
        if totfunc == None: totfunc = self.totfunc
        self.columns.append (twiki_column (desc, samp, formatter, totfunc,
                                           self.rowfunc_flag,
                                           for_width = for_width,
                                           just = just))
        return
    def _append_label (self, label):
        just = 'l'
        if label.startswith ('r:') or label.startswith ('m:') or label.startswith ('x:'):
            just = label[0]
            label = label[2:]
        self.just.append (just)
        self.labels.append (label)
        return
    def add_row (self, label, func):
        self._append_label (label)
        for c in self.columns:
            c.append_func (func)
        return
    def add_division (self, label):
        if self.tex:
            self._append_label ('m:' + label)
        else:
            self._append_label ('m:*' + label + '*')
        for c in self.columns:
            c.append_str('')
        return
    def add_literal (self, label):
        self._append_label ('x:' + label)
        for c in self.columns:
            c.append_str('')
        return
    def start_totals (self):
        self.total_first = len (self.labels)
        return
    def add_totals (self, label):
        self._append_label (label)
        for c in self.columns:
            c.total(self.total_first)
        return
    def dump (self, f=sys.stdout, first=0):
        lwid = max([len(s) for j,s in zip(self.just,self.labels) if j != 'x']) + 1
        cwid = [c.width() for c in self.columns]
        for i in range(first, len(self.labels)):
            if self.tex:
                line = ''
                lend = '\\\\'
                sep = '&'
            else:
                line = '|'
                lend = '|'
                sep = '|'

            if self.just[i] == 'm':
                if self.show_row_labels:
                    line += just(self.labels[i], 'l', lwid + sum(cwid)) + (sep * len(self.columns))
                else:
                    line += just(self.labels[i], 'l', sum(cwid)) + (sep * (len(self.columns)-1))
                line += lend
                print >> f, line
            elif self.just[i] == 'x':
                print >> f, self.labels[i]
            else:
                cols = [c.format_as_list(i, self.just[i]) for c in self.columns]
                ncrows = max ([len(c) for c in cols])
                for j in range(ncrows):
                    ssep = ''
                    if j == 0 and self.show_row_labels:
                        line += just(self.labels[i], self.just[i], lwid)
                        ssep = sep
                    for c in cols:
                        cc = c[j] if j < len(c) else ' '*len(c[0])
                        line += ssep + cc
                        ssep = sep
                    line += lend
                    print (line, file = f)
                    if self.tex:
                        line = ''
                    else:
                        line = '|'

        return

